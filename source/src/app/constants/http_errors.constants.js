angular.module('appConfig')
.constant('HTTP_ERRORS',
        {
            "0" : {
                title : "Server Unreachable",
                message : "Check the network connection and try again."
            },
            "-1" : {
                title : "Server Unreachable",
                message : "Check the network connection and try again."
            },

            "401" : {
                title : "",
                message : "Session has timed out. Please login to continue."
            },
            "403" : {
                title : "",
                message : "Access denied"
            },
            "404" : {
                title : "",
                message : "The requested URL was not found."
            },
            "408" : {
                title : "",
                message : "The system timed out waiting for a response from the server."
            },
            "500" : {},
            "INTERNAL_ERR" : {
                title : "Internal Error",
                message : "The system has encountered an unrecoverable error"
            },
            "ERROR_ASSISTANCE" : {
                message : "For further assistance, contact the administrator."
            },
            "UNKNOWN" : {
                title : "Unknown Response from Server",
                message : "For further assistance, contact the administrator."
            },
            "TIMEOUT" : {
                title : "Response Timeout",
                message : "The server is delayed in responding to the request. The operation may have failed."
            }
        }
);