angular.module('appConfig')
.constant('MIGRATOR_CONSTANTS',
{
	//Common constants used app wide
	"COMMON" : {
		"HTTP_SERVICE_CONFIG" : {
			"HEADER_KEY_ACCEPT":"Accept",
			"HEADER_KEY_CONTENT_TYPE":"Content-Type",
			"HEADER_VALUE_ACCEPT":"application/json",
			"HEADER_VALUE_CONTENT_TYPE":"application/json"
		},
		"UNSAVED_DIALOG_CONSTANTS" : {
				"TITLE" : "Unsaved Changes",
				"BODY" : "Leaving this screen will result in loss of unsaved changes.",
				"PRIMARY" : "STAY",
				"SECONDARY" : "LEAVE"
			},
		"LOSS_OF_CHANGES_DIALOG_CONSTANTS" : {
			"TITLE" : "Unsaved Changes",
			"BODY" : "Proceeding with {{ActionString}} will result in loss of unsaved changes.",
			"PRIMARY" : "CANCEL",
			"SECONDARY" : "PROCEED"
		},
		"DISCARD_CHANGES" : {
			"MESSAGE" : "All edited values have been discarded."
		},
		"FORMATTING" :{
			"UTC_DATE" : "yyyy/MM/dd@HH:mm",
			"LEADERBOARD_CALENDAR" : "EEE, dd MMM, yyyy",
			"CALENDAR_MINUTE_DETAIL_FORMAT" : "MMM dd, yyyy HH:mm",
			"CALENDAR_HOUR_DETAIL_FORMAT" : "MMM dd, yyyy HH:mm",
			"CALENDAR_DAY_DETAIL_FORMAT" : "MMM dd, yyyy",
			"LEFT_SIDEBAR_SERVER_TIME":"EEE  MMM  d  HH:mm",
			"STATUS_WIDGET_DATE":"EEE  MMM  d ",
			"STATUS_WIDGET_TIME":"HH:mm",
			"STATUS_WIDGET_TIMESTAMP":"EEE  MMM  d  HH:mm"

		},
		"EVENTS" : {
			"FILTER_ON_EVENTS" : "FiltersOnEvents"
		},
		"SYNC_PROJECTIONS" :{
			"PROJ_DISPLAY_STRING" : "Sync projections calculated at"
		}
	},
	//Job module constants
	"JOBS_MODULE": {
		"JOB_INVALID_ID" : "JOB_INVALID_ID",
		"GRID_CONSTANTS" : {
			"NO_RECORDS_MESSAGE" : "No jobs to display",
			"PROJECTIONS_NO_RECORDS_MESSAGE" : "Projections are still being calculated",
			"SYNC_HISTORY_NO_RECORDS_MESSAGE" : "No sync history to display"
		},
		"jobAlarms": {
			"SYNC_SRC_CONNECT_ERR": "Unable to connect to source host",
			"SYNC_SRC_AUTH_ERR": "Authentication failed on source host",
			"SYNC_SRC_INVALID_ARG_ERR": "Unable to locate source path on source host",
			"SYNC_DST_CONNECT_ERR": "Unable to connect to destination host",
			"SYNC_DST_AUTH_ERR": "Authentication failed on destination host",
			"SYNC_DST_INVALID_ARG_ERR": "Unable to locate destination path on destination host",
			"SYNC_SRC_NOT_SUPPORTED_ERR": "Operation not supported on source host",
			"SYNC_DST_NOT_SUPPORTED_ERR": "Operation not supported on destination host",
			"SYNC_INTERNAL_ERR": "Internal system error"
		},
		"DELETE_DIALOG_CONSTANTS" : {
			"TITLE" : "Delete Job is Irreversible",
			"BODY" : "This job and all its associated records will be permanently deleted.",
			"PRIMARY" : "CANCEL",
			"SECONDARY" : "DELETE JOB"
		},
		"SAVE_JOB_MISSING_ESTIMATES_DIALOG_CONSTANTS" : {
			"TITLE" : "Missing Estimates for Initial Sync Projection Calculations",
			"BODY" : "Sync Projections will only be available after a few successful syncs if this job is updated without any Estimates for Initial Sync Projection Calculations.",
			"PRIMARY" : "CANCEL",
			"SECONDARY" : "UPDATE JOB"
		},
		"CREATE_JOB_MISSING_ESTIMATES_DIALOG_CONSTANTS" : {
			"TITLE" : "Missing Estimates for Initial Sync Projection Calculations",
			"BODY" : "Sync Projections will only be available after a few successful syncs if this job is created without any Estimates for Initial Sync Projection Calculations.",
			"PRIMARY" : "CANCEL",
			"SECONDARY" : "CREATE JOB"
		}
	},

	//Host module constants
	"HOSTS_MODULE": {
		"HOST_INVALID_ID" : "HOST_INVALID_ID",
		"GRID_CONSTANTS" : {
			"NO_RECORDS_MESSAGE" : "No hosts to display",
			"PROJECTIONS_NO_RECORDS_MESSAGE" : "Projections are still being calculated",
			"SYNC_HISTORY_NO_RECORDS_MESSAGE" : "No sync history to display",
			"NO_PENDING_JOBS_MESSAGE" : "There are no pending jobs on this host."
		},
		"hostAlarms": {
			"HOST_CONN_ERR": "Unable to reach host",
			"HOST_AUTH_ERR": "Authentication error",
			"HOST_NO_NDMP_SERVICE": "No NDMP service",
            "HOST_NDMP_ERR": "NDMP service returned an error"
		},
		"DELETE_DIALOG_CONSTANTS" : {
			"TITLE" : "Delete Host is Irreversible",
			"BODY" : "Deleting a host will also permanently delete all jobs related to the host.",
			"PRIMARY" : "CANCEL",
			"SECONDARY" : "DELETE HOST"
		}
	},


	//Events module constants
	"EVENTS_MODULE": {
		"GRID_CONSTANTS" : {
			"NO_RECORDS_MESSAGE" : "No events to display"
		}
	},



	//Login module constants
	"LOGIN_MODULE": {},



	//Users module constants
	"USERS_MODULE": {
		"GRID_CONSTANTS" : {
			"NO_RECORDS_MESSAGE" : "No users to display"
		},
		"DELETE_DIALOG_CONSTANTS" : {
			"TITLE" : "Delete User is Irreversible",
			"BODY" : "User {{UserName}} will be permanently deleted. Jobs and hosts created by user {{UserName}} will not be deleted.",
			"PRIMARY" : "CANCEL",
			"SECONDARY" : "DELETE USER"
		}
	},


	"DEFAULT_CREDENTIALS" :{},

	"MIGRATOR_SERVER" : {}

}).constant('MIGRATOR_EVENTS', {
    INTERTAB_REFRESH : "WIDGET_REFRESH",
    MIGRATOR_RELOAD:"MIGRATOR_RELOAD",
    FORCE_REFRESH : "WIDGET_REFRESH"
}); 