angular.module('appConfig')
.constant('APP_CONFIG', {
    name: 'Fire.io',
    logo: 'assets/images/logo.png',
    baseWebApiUrl: '/twuiztApp',
    inactivityTimeout : (60000 * 15),
    version: '0.1.0',
    defaultSkin: 'battleship-grey',
    colors:{
              "lightRed" : "rgb(237, 185, 175)",
              "green": "#70BF41",
              "blue": "rgba(33,150,243,0.87)",
              "darkGrey": "#53585F",
              "lightGrey": "#A6AAA9",
              "ultraLightGrey": "rgba(83, 88, 95, 0.19)",
              "lightGreen":"rgba(132, 192, 239, 0.870588)",
              "transparentGrey":"rgba(205, 210, 210,0.5)",
              "grey":"rgba(161,161,162,0.7)",
              "ultraLightBlue":"rgba(92, 174, 239, 0.870588)"

          },
    defaultUser: 'migrator',
    pages : {
        'twuizt':'/twuizt',
        'defaultPage':  '/twuizt/leaderboard',
        'loginPage' : '/login',
        'leaderboardPage':'/twuizt/leaderboard',
        'arenaPage':'/twuizt/arena',
        'wizardPage':'/twuizt/wizard',
        'redirect':'/redirect'
    },
    'rolesAlllowedForStates' :{
      'admin-panel.authenticated.leaderboard':["0","1"],
      'admin-panel.authenticated.arena':["0","1"],
      'admin-panel.authenticated.wizard':["0"]
    } ,
    'nonCachedStates':['redirect', 'base', 'baseAbstract', 'login'],
    'states':  {
        'redirect':'redirect',
        'baseAbstract' : 'admin-panel',
        'base' : 'admin-panel.authenticated',
        'defaultState':'admin-panel.authenticated.arena',
        'login':'login',
        'arena' : 
        {
          'main' : 'admin-panel.authenticated.arena',

        }
    },
    refreshTimes:{
        leaderboard:{
          baseInterval : 60000,
          performanceWidget: {
            enabled:true,
            delay:0
          },
          odometerWidgets: {
            enabled:true,
            delay:1000
          },
          wordCloudWidget: {
            enabled:true,
            delay:2000
          },
          throughputWidget: {
            enabled:true,
            delay:2000
          },
          calendarWidget: {
            enabled:true,
            delay:2000
          },
          dataTransferredWidget: {
            enabled:true,
            delay:2000
          }
        },
        users:{
            events:{
              refreshInterval:20000  
            },
            main:{
              refreshInterval:20000    
            }            
        }
    }
});