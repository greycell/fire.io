'use strict';

/**
* @ngdoc directive
* @name inlineMessage
* @restrict A
* @scope
*
* @description
*
* Handles the rendering of inline messages in migrator
*
* @usage
* ```html
* <div inline-message></div>
* ```
*/
angular.module('twuizt')
.directive('inlineMessage', [function(){
    return {
        restrict: 'E',
        replace:false,
        priority: Number.MIN_SAFE_INTEGER, //Lowest priority
        templateUrl: 'app/directives/inline-message/inline-message.tmpl.html',
        scope: {
            "types":"=types",
            "messages":"=messages"
        },
        controller: ['$scope', '$interval', function($scope, $interval) {
          console.info("Migrator inline message controller is loaded : ",$scope);

            function initialize(types)
            {
                var keysList =  types == null ? {} : Object.keys(types);
                var keyMap = [];
                var i;
                for(i=0;i<keysList.length;i++)
                {
                    keyMap.push({
                        name:keysList[i],
                        id:i
                    });
                }

                $scope.errors = {
                    keys:keyMap,
                    flags:types,
                    messages:$scope.messages
                };
            }

            initialize($scope.types);

            $scope.$watch('types', function(newValue, oldValue){
                if(newValue == oldValue)
                {
                    return;
                }
                
                $scope.errors.flags = $scope.types;

                checkForKeyMapping(newValue);

            }, true);

            function checkForKeyMapping(types)
            {
                for(var type in types)
                {
                    if($scope.types[type] == null)
                    {
                        initialize(types);
                        return;
                    }
                }
            }

           }],
        link: function(scope, element, attrs, ctrl) {
            console.info("Migrator inline message directive is linked", scope, element, scope.elementObj);
            // if(scope.lazy != "true" && scope.lazy != true)
            // {
            //     console.error("Lazy mode is off : "+scope.lazy);
            //     scope.callback();
            // }
        }
    };
}])
.directive('whenReady', ['$rootScope', '$interval', function($rootScope, $interval){
return {
            restrict: "A",
            priority: Number.MIN_SAFE_INTEGER, //Lowest priority
            link    : function(scope, element, attr){
                        $interval(function(){
                            console.error("finished loading.", scope, element, attr);
                            if(attr.whenReady == "immediate")
                            {
                                console.error("Callback function to initialize ng messages");
                                scope.callback();
                            }
                            else
                            {
                                console.error("Emitting event for intializing ng messages");
                                scope.$broadcast("InitializeNgMessagesEvent");
                            }
                        }, 0,1);
            }
        };
}]);