'use strict';

/**
* @ngdoc directive
* @name migratorBreadcrumbs
* @restrict A
* @scope
*
* @description
*
* Handles the navigation of the current view to its parent view.
*
* @usage
* ```html
* <h2 migrator-bread-crumbs></h2>
* ```
*/
angular.module('twuizt')
.directive('twuiztBreadcrumbs', ['$state', function ($state) {
    return {
        restrict: 'A',
        //require: '^toolbar',
        scope:true,
        transclude: true,
        replace: true,
        templateUrl: 'app/directives/breadcrumbs/breadcrumbs.tmpl.html',
        controller: ['$scope', '$rootScope', '$timeout',  function($scope, $rootScope, $timeout) {
            console.log("Linking the twuizt breadcrumbs.");

            $scope.stateObj = {};
            $scope.stateObj.oldStates = {};
            $scope.changeState = function(state)
            {
        		console.log("Changing state to : "+state);
                $state.go(state);
            };

            console.log("Fetching breadcrumb ");
            var tag;

            console.info("$stateChangeSuccess current : "+$state.$current.self.name);

            var oldStates = []
            var parent = $state.$current.parent;
            while(parent != null)
            {
              var parentName = parent.self.name;
              var alias = parent.self.data.alias;
              console.info("$stateChangeSuccess parent : "+parentName);
              if(parentName == "admin-panel.default")
              {
                break;
              }

              tag = {
                    stateIcon:null,
                    hoverIcon:null,
                    intraIcon:null,
                    stateIconClass:null,
                    hoverIconClass:null,
                    intraIconClass:null,
                    alias:alias,
                    main:null,
                    detail:null,
                    stateName:parentName
                };

              parent = parent.parent;
              oldStates.unshift(tag);
            }


            $scope.stateObj.oldStates = oldStates;
        }]
    };
}]);