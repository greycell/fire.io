'use strict';

/**
* @ngdoc directive
* @name migratorPagetitle
* @restrict A
* @scope
*
* @description
*
* Handles the navigation of the current view to its parent view.
*
* @usage
* ```html
* <h2 migrator-page-title></h2>
* ```
*/
angular.module('twuizt')
.directive('twuiztPagetitle', ['$state', '$rootScope', function ($state, $rootScope) {
    return {
        restrict: 'A',
        //require: '^toolbar',
        replace: true,
        templateUrl: 'app/directives/pagetitle/pagetitle.tmpl.html',
        controller: ['$scope', '$rootScope', '$timeout', '$state', 'MigratorStateManager', function($scope, $rootScope, $timeout, $state, MigratorStateManager) {
            console.log("Linking the twuizt breadcrumbs.");

            $scope.titles = {};
            $scope.titles = MigratorStateManager.titles;

            $scope.checkActiveStateForTitleDisplay = function(STATE)
            {
                // console.log("Checking current state for title :"+$state.current.name+" = '"+ JSON.stringify(STATE)+"' is active.");
                if($state.current.name == STATE.stateName)
                {

                  //console.log(STATE.stateName+" is active.");
                  var strPageTitle ="";

                  strPageTitle="Migrator - " + strPageTitle;
                  //console.log("Initial/Prev strPageTitle::"+strPageTitle);
                  if((""==STATE.main || null==STATE.main))
                  {
                    strPageTitle+= STATE.alias;
                  }
                  else
                  {

                    //console.log(":::::strPageTitle::"+strPageTitle);
                    strPageTitle = "Migrator - "+STATE.main;

                    if(STATE.detail != null && STATE.detail != "")
                    {
                      strPageTitle += ":"+STATE.detail;
                    }

                    //console.log("Setting :: strPageTitle:::"+strPageTitle);
                  }

                $rootScope.strTitle=strPageTitle;
                //console.log("TODO : Other modules(other than Jobs)"+$rootScope.strTitle);

                return false;
              }
                //console.log("STATE.stateName:: is not active.");
                return true;
            };
        }]
    };
}]);
