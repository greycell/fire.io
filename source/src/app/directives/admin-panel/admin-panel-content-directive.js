'use strict';

/**
* @ngdoc directive
* @name adminPanelContent
* @restrict E
* @scope
*
* @description
*
* Handles injection of the footer into the admin panel content
*
* @usage
* ```html
* <div ui-view="content" admin-panel-content></div>
* ```
*/
angular.module('twuizt')
.directive('adminPanelContent', ['$compile', '$templateRequest', 
    function($compile, $templateRequest) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.$on('$stateChangeStart', function() {
                var mdContentElement = element.parent();
                // scroll page to the top when content is loaded (stops pages keeping scroll position even when they have changed url)
                mdContentElement.scrollTop(0);
            });
        }
    };
}]);