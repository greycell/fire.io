'use strict';

/**
* @ngdoc directive
* @name sideMenuItem
* @restrict E
* @scope
*
* @description
*
* Simple menu link item
*
* @usage
* ```html
* <side-menu-item ng-repeat="item in menu" item="item"></side-menu-item>
* ```
*/
angular.module('twuizt')
.directive('sideMenuItem', ['$state', '$document', '$rootScope', '$interval',
    'UserSession', 'MigratorStateManager',
    function($state, $document, $rootScope, $interval,UserSession, MigratorStateManager) {
    return {
        restrict: 'E',
        require: '^sideMenu',
        scope: {
            item: '='
        },
        template: '<div ng-include="itemTemplate"></div>',
        controller: function($scope, $location) {
            // load a template for this directive based on the type ( link | dropdown )
            $scope.itemTemplate = 'app/directives/side-menu/side-menu-' + $scope.item.type + '.tmpl.html';
            $scope.item.url = $state.href($scope.item.state);
            /***
            * Menu Click Handlers
            ***/
            $scope.toggleMenu = function() {
                // send message down the menu from the parent, item is toggled
                // this will close any sibling menus
                $scope.$parent.$parent.$broadcast('toggleMenu', $scope.item, !$scope.item.open);
            };

            var item = $scope.item;
            item.enable =false;
            var userRoles = UserSession.getUser().roles;
            if(item.access)
            {
                var result = MigratorStateManager.allowUserAccess(item.access,userRoles);
                if( result != null && result != false)
                {
                    item.enable = true;
                }
             }   

                // for(var i=0;i<item.access.length;i++)
                // {
                //     for(var j=0;j<userRoles.length;j++)
                //     {
                //           if(item.access[i] == userRoles[j])
                //           {
                //             item.enable=true;
                //             break;
                //           }

                //     }
                //     if(item.enable)
                //     {
                //         break;
                //     }    
                // }       
            $scope.openNewMigratorWindow = function(state)
            {
                $document[0].activeElement.blur();
                $interval(function(){
                    $rootScope.$broadcast("CloseLeftSideMenu", null);
                },100,1);
                console.log("Opening new window with state : "+state);
                var urlToOpen = $scope.item.url;
                console.log("url to open : "+$location.origin+"/#"+urlToOpen);
                //window.open(urlToOpen,"_blank","width=500");
                window.open(urlToOpen, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=500, width=1000, height=600");
            }

            // this event ensures that any sibling menu items are closed when menu item is opened
            $scope.$on('toggleMenu', function(event, item, open) {
                // if this is the item we are looking for
                if($scope.item === item) {
                    $scope.item.open = open;
                }
                else {
                    $scope.item.open = false;
                }
            });

            /***
            * URL Change Handlers
            ***/

            function isActive() {
                var params = $scope.item.params === undefined ? {} : $scope.item.params;
                return $state.includes($scope.item.state, params);
            }

            // on first init check if we are the current menu item
            if(isActive()) {
                openMenu();
            }

            // opens the menu then calls its parents to also open
            function openMenu() {
                $scope.item.active = true;
                $scope.item.open = true;
                $scope.$emit('openParents');
            }

            // add a watch for when the url location changes
            $scope.$on('$locationChangeSuccess', function() {
                // location has changed so update the menu
                $scope.item.active = false;
                $scope.item.open = false;
                if(isActive()) {
                    openMenu();
                }
            });

            // adds an extra hue class if the item is active
            $scope.activeClass = function() {
                return //isActive() ? 'md-hue-1' : '';
                  isActive() ? '' : '';
            };

            $scope.openLink = function() {
                // if we dont have any default params for this state just use empty object
                var params = $scope.item.params === undefined ? {} : $scope.item.params;
                console.log("Current location : "+JSON.stringify(location.hash));
                if($scope.item.url == location.hash)
                {
                    console.log("Menu item is already active. Broadcasting event 'CloseLeftSideMenu'");
                }
                
                $scope.$emit('CloseLeftSideMenu');

                $interval(function(){
                    $state.go($scope.item.state, params);
                },500,1);
            };

            // this event is emitted up the tree to open parent menus
            $scope.$on('openParents', function() {
                // openParents event so open the parent item
                $scope.item.active = true;
                $scope.item.open = true;
            });
        }
    };
}]);
