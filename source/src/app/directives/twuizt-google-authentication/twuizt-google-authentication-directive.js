'use strict';

/**
* @ngdoc directive
* @name countupto
* @restrict A
* @scope
*
* @description
*
* Animated counting number
*
* @usage
* ```html
* <twuizt-google-authentication></twuizt-google-authentication>
* ```
*
*
*/
angular.module('twuizt')
.directive('twuiztGoogleAuthentication', ['$interval', '$window', '$rootScope', function($interval, $window, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            config:'=config'
        },
        link:function(scope, element, attrs) {
            console.log("Twuizt google authentication directive is linked and loaded.");
            var googleAuthScript = document.createElement('script'); 
            googleAuthScript.type = 'text/javascript'; 
            googleAuthScript.async = true;
            var configStr = JSON.stringify(scope.config);
            scope.__twuiztGoogleAuthConfig = scope.config;
            console.log("Google Auth config : "+JSON.stringify(scope.__twuiztGoogleAuthConfig));

            var googleAuthFunctionName = "googleAuthCallBack"+parseInt(Math.random()*100000000000);
            console.error("Google auth : "+googleAuthFunctionName);
            $window[googleAuthFunctionName] = function()
            {
                console.warn("Loading auth2 library");

                gapi.load('auth2', function(){
                      // Retrieve the singleton for the GoogleAuth library and set up the client.
                    console.warn("Initializing google auth with parameters");
                    gapi.auth2.init(angular.element("twuizt-google-authentication").scope().twuiztGoogleAuthConfig);
                    console.log("Google auth call back executed with arguments : ",arguments);
                    $rootScope.$broadcast('GoogleApiLoadedEvent');
                    // $window[googleAuthFunctionName] = undefined;
                });
            };

            // googleAuthScript.src = 'https://apis.google.com/js/client.js?onLoad="'+callback.toString()+'"';
            // googleAuthScript.src = 'https://apis.google.com/js/client:platform.js?onload="'+googleAuthCallbackFunc.toString()+'"';

            var src = 'https://apis.google.com/js/api:client.js?onload='+googleAuthFunctionName;
            googleAuthScript.src = src;
            var s = document.getElementsByTagName('twuizt-google-authentication')[0]; 
            s.appendChild(googleAuthScript);
        }
        ,
        controller : ['$rootScope', '$scope', function($rootScope, $scope){
            console.log("Twuizt google authentication controller is loaded.");
        }]

    };
}]);