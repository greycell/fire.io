'use strict';

/**
* @ngdoc directive
* @name migratorToolbarTitle
* @restrict A
* @scope
*
* @description
*
* Handles the icon set configuration of the current view.
*
* @usage
* ```html
* <h2 toolbar-action-buttons></h2>
* ```
*/
angular.module('twuizt')
.directive('toolbarActionIcons', ['$state', '$rootScope', 
    'MIGRATOR_EVENTS', 
    function ($state, $rootScope,
        MIGRATOR_EVENTS) {
    return {
        restrict: 'A',
        //require: '^toolbar',
        replace: true,
        templateUrl: 'app/directives/migrator-toolbar-action-buttons/toolbar-action-icons.tmpl.html',
        controller: ['$scope', '$rootScope', '$state', 'MigratorStateManager', function($scope, $rootScope, $state, MigratorStateManager) {
            console.log("Linking the migrator action buttons.");

            $scope.checkActiveStateForActionIcons = function(STATE) 
            {
                // console.log("###Checking current state for actions icons :"+$state.current.name+" = '"+STATE.name+"'");
                if($state.current.name == STATE.name)
                {
                    // console.log(STATE.name+" is active.###");
                    return false;
                }
                // console.log(STATE.name+" is not active.###");
                return true;
            };

            $scope.handleActionIconClick = function(event, action)
            {
            	console.log("Broadcasting action button click event for : "+action);
            	$rootScope.$broadcast(MIGRATOR_EVENTS.ACTION_BUTTON_CLICK, action, event)
            };

            $scope.states = {};
            $scope.states = MigratorStateManager.states;
            console.log("Successfully linked action icons : "+JSON.stringify($scope.states));

        }]
    };
}]);