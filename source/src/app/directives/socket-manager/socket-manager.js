'use strict';

angular.module('twuizt')
.service('SocketManager', ['socket', '$rootScope', function(socket, $rootScope)
{

    var socketManager = this;

    this.init = function(userSession){
        var userObj = userSession.getUser();
        var options = {
            path: '/twuiztSocket', 
            transports:['polling','websocket'],
            query:{
                userId: userObj.userId
            }
        };
        this.socket = socket({
            prefix: '',
            ioSocket: io.connect('', options)
        });

        if(userObj.isAdmin){
            this.socket.on('twuiztAdmin', function (ev, data) {
                switch(ev)
                {
                    case "threat_alert":
                    case "userStats":
                    case "refresh":
                    console.log("Socket event : "+ev, data);
                    $rootScope.$broadcast("STAT_RELOAD_EVENT", data);
                    break;
                }
            });
        }
        else{
            this.socket.on('twuiztClient', function (ev, data) {
                switch(ev)
                {
                    case "reload":
                    console.log("Socket event : "+ev, data);
                    break;

                    case "refresh":
                    console.log("Socket event : "+ev, data);
                    $rootScope.$broadcast("STAT_RELOAD_EVENT", data);
                    break;
                }
            });
        }

    };

    $rootScope.$on('twuiztAppEvent', function (ev, data) {
        console.error("Socket event : ",data);
    });


    $rootScope.$on('TwuiztSocketerror', function (ev, data) {
        console.error("Socket event : ",data);
    });


    $rootScope.$on('twuiztAppEvent', function (ev, data) {
        console.error("Socket event : ",data);
    });

    // $rootScope.$on('TwuiztSocket:twuiztAppEvent', function (ev, data) {
    //     console.error("Socket event : ",data);
    // });

    // $rootScope.$on('TwuiztSocket:twuiztAppEvent', function (ev, data) {
    //     console.error("Socket event : ",data);
    // });
}]);