'use strict';

/**
* @ngdoc directive
* @name triLoader
* @restrict E
* @scope
*
* @description
*
*
* @usage
* ```html
* <migrator-loader></migrator-loader>
* ```
*/
angular.module('twuizt')
.directive('migratorUiLoader', ['$rootScope', function($rootScope) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        // template: '<md-content flex class="migrator-ui-loader" ng-show="active" layout="column" layout-fill layout-align="start start"><md-progress-linear style="width:300%;" md-mode="indeterminate"></md-progress-linear></md-content>',
        template: '<md-content flex class="migrator-ui-loader" ng-show="active" layout="column" layout-fill layout-align="start start"><md-progress-linear class="md-primary migrator-loader-style" md-mode="query"></md-progress-linear></md-content>',
        link: function(scope, element, attrs) {
            // scope.appName = "Loading";
            console.log('Migrator ui loader directive init');
            scope.active = false;

            $rootScope.$on('ToggleMigratorUiLoader', function(evt, next, current)
            {
                if(next == true)
                {
                    scope.active = true;
                }
                else
                {
                    scope.active = false;
                }
            });
        }
    };
}]);