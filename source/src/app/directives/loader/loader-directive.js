'use strict';

/**
* @ngdoc directive
* @name triLoader
* @restrict E
* @scope
*
* @description
*
* Adds a loader screen that takes up 100%
*
* @usage
* ```html
* <tri-loader></tri-loader>
* ```
*/
angular.module('twuizt')
.directive('triLoader', ['$rootScope', 'APP_CONFIG', 
    function($rootScope, APP_CONFIG) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        template: '<md-content flex class="migrator-content-loader" ng-show="active" layout="column" layout-fill layout-align="center center"><div class="loader-inner"><md-progress-circular md-mode="indeterminate" md-diameter="90"></md-progress-circular></div><h3 class="md-headline">{{appName}}</h3></md-content>',
        link: function(scope, element, attrs) {
            scope.appName = APP_CONFIG.name;
            console.log('loader directive init');
            scope.active = true;

            $rootScope.$on('$viewContentLoading', function() {
                scope.active = true;
            });

            $rootScope.$on('$viewContentLoaded', function() {
                scope.active = false;
            });

            $rootScope.$on('ToggleMigratorUiLoader', function(evt, next, current)
            {
                if(next == false)
                {
                    scope.active = false;
                }
            });
        }
    };
}]);