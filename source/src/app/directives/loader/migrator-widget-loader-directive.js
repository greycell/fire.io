'use strict';

/**
* @ngdoc directive
* @name triLoader
* @restrict E
* @scope
*
* @description
*
*
* @usage
* ```html
* <migrator-loader></migrator-loader>
* ```
*/
angular.module('twuizt')
.directive('migratorWidgetLoader', ['$rootScope', 
    function($rootScope) {
    return {
        restrict: 'E',
        scope: {
            "diameter":"=diameter",
            "top":"=top",
            "bottom":"=bottom",
            "left":"=left",
            "right":"=right",
            "name":"@name"
        },
        template:   '<div id="{{name}}" class="migrator-widget-loader" ng-show="active" style="position: absolute; z-index:999999; top:{{top}}px; left:{{left}}px; position:absolute; background:rgba(255, 255, 255, 0.3); width:{{parentWidth}}px; height:{{parentHeight}}px;">'
                        +'<div style="position: absolute; top: calc(50% - {{diameter/2}}px);left: calc(50% - {{diameter/2}}px);">'
                            +'<md-progress-circular style="opacity:0.7;" class="md-primary md-hue-3" md-mode="indeterminate" md-diameter="{{diameter}}" >'
                            +'</md-progress-circular>'
                        +'</div>'
                    +'</div>',
        link: function(scope, element, attrs) {
            // scope.appName = "Loading";
            console.log('Migrator widget loader directive init :: ',scope, element, attrs);
            console.log('Migrator widget loader directive init linked element '+scope.name+' :: ',element[0]);
            scope.active = false;
            scope.widthOffset = scope.left + scope.right;
            scope.heightOffset = scope.bottom + scope.top;
        },
        controller : ['$rootScope', '$scope', function($rootScope, $scope){
            
            console.log('Migrator widget loader directive init post linked Element '+$scope.name);
            $scope.containerParent = null;
            $rootScope.$on('ToggleMigratorWidgetLoader', function(evt, action, current)
            {
                console.warn("ToggleMigratorWidgetLoader :: ",action);
                if($scope.containerParent == null)
                {
                    $scope.containerParent = angular.element("#"+$scope.name).parent().parent();
                }

                if(action.name != $scope.name)
                {
                    return;
                }

                $scope.parentHeight = $scope.containerParent.height();
                $scope.parentWidth = $scope.containerParent.width();
                
                console.log('Migrator widget loader directive init post linked Element '+$scope.name+' :: '+$scope.containerParent.width());

                if(action.activate == true)
                {
                    $scope.active = true;
                }
                else
                {
                    $scope.active = false;
                }
            });
        }]
    };
}]);