'use strict';

/**
* @ngdoc directive
* @name triLoader
* @restrict E
* @scope
*
* @description
*
*
* @usage
* ```html
* <migrator-loader></migrator-loader>
* ```
*/
angular.module('twuizt')
.directive('migratorApiLoader', ['$rootScope', 
    function($rootScope) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        template: '<md-content flex class="migrator-api-loader" ng-show="active" ng-hide="!active" layout="column" layout-fill layout-align="center center"><div class="loader-inner"><md-progress-circular md-mode="indeterminate" md-diameter="90"></md-progress-circular></div><h3 class="md-headline"></h3></md-content>',
        link: function(scope, element, attrs) {
            // scope.appName = "Loading";
            console.log('Migrator ui loader directive init');
            scope.active = false;

            $rootScope.$on('ToggleMigratorApiLoader', function(evt, action, current)
            {
                if(action == true)
                {
                    scope.active = true;
                }
                else
                {
                    scope.active = false;
                }
            });
        }
    };
}]);