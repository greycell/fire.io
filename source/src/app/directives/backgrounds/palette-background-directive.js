'use strict';

/**
* @ngdoc directive
* @name paletteBackground
* @restrict A
* @scope
*
* @description
*
* Adds a palette colour and contrast CSS to an element
*
* @usage
* ```html
* <div palette-background="green:500">Coloured content</div>
* ```
*/
angular.module('twuizt')
.directive('paletteBackground', ['MigratorTheming', function (MigratorTheming) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var splitColor = attrs.paletteBackground.split(':');
            var color = MigratorTheming.getPaletteColor(splitColor[0], splitColor[1]);

            if(color !== undefined) {
                element.css({
                    'background-color': MigratorTheming.rgba(color.value),
                    'border-color': MigratorTheming.rgba(color.value),
                    'color': MigratorTheming.rgba(color.contrast)
                });
            }
        }
    };
}]);
