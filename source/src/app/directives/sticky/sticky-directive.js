'use strict';

/**
* @ngdoc directive
* @name sticky
* @restrict A
* @scope
*
* @description
*
* Makes content sticky (flloat over other contents)
*
* @usage
* ```html
* <div sticky></div>
* ```
*/
angular.module('twuizt')
.directive('sticky', ['$mdSticky', 
    function($mdSticky) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            $mdSticky(scope,element);
        }
    };
}]);