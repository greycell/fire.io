
'use strict';

/**
 * @ngdoc module
 * @name migrator.arena
 * @description
 *
 * The `migrator.arena` module adds the migrator arena page
 */
angular.module('twuizt.arena', ['nvd3', 'gridster', 'ui.odometer', 'ui.grid'])
.config([ '$stateProvider', 'APP_CONFIG', 
    function ($stateProvider, APP_CONFIG) {

    $stateProvider
        .state(APP_CONFIG.states.arena.main, {
        url: '/arena',
        data : {
            alias: "Arena"
        },
        resolve:{
          STATE:function()
          {
              return {
                pageName : "Arena",
                stateName : APP_CONFIG.states.arena.main,
                actionIcons : [
                {
                    iconName : "insert_chart",
                    action : "show_stats",
                    iconColor:'MessagesTheme-primary-500',
                    iconClass : null,
                    activeClass : null,
                    activeColor:null,
                    isActivated : false,
                    hidden : false,
                    tooltip: "Show Stats"
                  }
              ]
              };
          }
        },
        views: (function(){
            var BASE = APP_CONFIG.states.base;

            var views = {};
            views["breadcrumb@"+BASE] = {
                template:"<h2 flex migrator-breadcrumbs></h2>"
            };
            views["content"] = { 
                templateUrl: 'app/modules/arena/arena.tmpl.html',
                controller: 'arenaController'
            };

            views["sidebarRight"] = { 
                templateUrl: 'app/modules/arena/arena-sidebar-right.tmpl.html',
                controller: 'arenaFilterController'
            };

            return views;
        })(),
        onEnter: function($stateParams) {
            console.info("entering current state with params : "+JSON.stringify($stateParams));
            console.info("entering current state with this : "+JSON.stringify(this));
        },
        onExit: function($stateParams) {
            console.info("exiting current state with params : "+JSON.stringify($stateParams));
        }

    })
}])
.run(function(SideMenu, APP_CONFIG) {
    SideMenu.addMenu({
        name: 'Arena',
        state: APP_CONFIG.states.arena.main,
        type: 'link',
        icon: 'whatshot',
        access:APP_CONFIG.rolesAlllowedForStates[APP_CONFIG.states.arena.main],
        detach:true,
        priority: 0.1
    });
    SideMenu.addMenu({
        type: 'divider',
        priority: 0.2
    });
});