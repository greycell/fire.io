'use strict';

/**
 * @ngdoc function
 * @name arenaFilterController
 * @module migrator.arena
 * @kind function
 *
 *
 */
angular.module('twuizt.arena').
controller('arenaFilterController', ['$scope', '$rootScope', '$timeout', '$state', '$mdMedia', '$mdSidenav',
    'STATE',
    'MIGRATOR_EVENTS', 'APP_CONFIG',
    'MigratorStateManager', 'SideNavService', 
     function($scope, $rootScope, $timeout, $state, $mdMedia, $mdSidenav,
        STATE,
        MIGRATOR_EVENTS, APP_CONFIG,
        MigratorStateManager, SideNavService) {
   
    console.log("arena filter controller is loaded.");
    $scope.rightSideBar = {
      activatedPanel : null  
    };

    $scope.arenaSidenav = {};
    $scope.arenaSidenavHeader = "";
    $scope.selectedHostsModel = null;
    $scope.selectedJobsModel = null;

    var showStatsButton = MigratorStateManager.getIconState(APP_CONFIG.states.arena.main, "show_stats");

     $scope.$watch(function() { return $mdMedia('gt-md'); }, function(value) {
        if(value == true)
        {
            showStatsButton.hide();
        }
        else
        {
            showStatsButton.show();
        }
      });

    $scope.$on(MIGRATOR_EVENTS.ACTION_BUTTON_CLICK, function(broadcastEvent, action, actionEvent)
    {
        console.info("Caught action button click event : "+action);
        if(action == 'show_stats')
        {
            $mdSidenav('arena-right-side-nav').toggle();
        }
    });

    $scope.openHostDetails = function(hostId)
    {
        $timeout(function(){
            $state.go(APP_CONFIG.states.hosts.details, {id:hostId});
        },0);
    }

    $scope.openJobDetails = function(jobId)
    {
        $timeout(function(){
            $state.go(APP_CONFIG.states.jobs.details, {id:jobId});
        },0);
    }

    $scope.$on(MIGRATOR_EVENTS.WIDGET_SIDENAV_ACTIVATE_EVENT, function(broadcastEvent, action, actionEvent)
    {
        console.info("Activating sidenav for data display from widget : "+action);
        var panelToActivate = null;
        switch(action)
        {
            case "hosts.alarms":
            panelToActivate = "HOSTS_ALARMS";
            break;
            case "jobs.alarms":
            panelToActivate = "JOBS_ALARMS";
            break;
            case "hosts.completed":
            case "hosts.streaming":
            case "hosts.idle":
            case "hosts.noJobs":
            panelToActivate = "HOSTS";
            break;
            case "jobs.completed":
            case "jobs.streaming":
            case "jobs.idle":
            case "jobsCompletingSoon":
            panelToActivate = "JOBS";
            break;

            default:
            return;
        }

        console.log("Activating right side nav for ", panelToActivate);
        var properties = action.split(".");
        var model = $scope.arenaSidenav;
        for(var i =0; i < properties.length; i++)
        {
            model = model[properties[i]];
        }

        if(panelToActivate == "JOBS")
        {
            $scope.selectedJobsModel = model;
        }
        else if(panelToActivate == "HOSTS")
        {
            $scope.selectedHostsModel = model;
        }
        
        $scope.arenaSidenavHeader = model.header;
        
        if(action == "jobsCompletingSoon")
        {
            $scope.arenaSidenavHeader = $scope.arenaSidenavHeader + model.dateToShow;
        }

        handleRightSideMenuOpenEvent(panelToActivate);
    });

    function handleRightSideMenuOpenEvent(settings)
    {

        SideNavService.handleRightSideMenuOpenEvent($scope.rightSideBar.activatedPanel, settings)
        .then(function(activatedPanel){
            $scope.rightSideBar.activatedPanel = activatedPanel;
        });
    };

}]);
