'use strict';
angular.module('twuizt.arena').
controller('AdminButtonSequenceController', 
    ['$scope', '$mdDialog', '$mdMedia', '$interval',
    'TwuiztNotifier', 'WizardService', 'SETTINGS',
    function($scope, $mdDialog, $mdMedia, $interval,
    TwuiztNotifier, WizardService, SETTINGS) {
    console.log("Preparing to initialize buttons sequence settings controller.");

    $scope.$mdMedia = $mdMedia;
    $scope.mode = SETTINGS.action;
    $scope.pattern = angular.copy(SETTINGS.pattern);

    $scope.buttonMatrix = SETTINGS.adminSettings.buttonMatrix;
    $scope.cancelPatternSettings = function(){
        console.log("cancelling button settings");
        $mdDialog.hide();
    };

    $scope.resetDuplicateError = function(){
        delete $scope.adminSettingsForm.patternString.$error['duplicate'];
    };

    $scope.updatePatternSettings = function(pattern){
        console.log("saving button settings");
        //Send request to edit the team.
        var payload = {
            action:SETTINGS.action,
            data: pattern
        };

        WizardService.updatePattern(payload)
        .then(function(response){
            console.log("Pattern update successful");
            if(response.isApiError && response.apiStatus == "DUPLICATE")
            {
                // TwuiztNotifier.showInfo("Pattern already exists. Enter a unique pattern.");
                $scope.adminSettingsForm.patternString.$error['duplicate'] = true;
                return;
            }
            TwuiztNotifier.showInfo("Pattern has been successfully "+SETTINGS.action+"ed.");
            $mdDialog.hide();
        },
        function(response){
            console.error("Pattern update failed.");
            // $mdDialog.cancel();
        });   
    }
}]);

