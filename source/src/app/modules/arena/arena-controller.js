'use strict';
/**
 * @ngdoc function
 * @name arenaController
 * @module migrator.arena
 * @kind function
 *
 *
 */
angular.module('twuizt.arena').
controller('arenaController', ['$scope', '$rootScope', '$filter', '$timeout', '$interval', '$state', '$mdMedia', '$mdDialog',
    'STATE',
    'APP_CONFIG', 'MIGRATOR_EVENTS', 'MIGRATOR_CONSTANTS',
    'MigratorStateManager',
    'MigratorLoader',
    'TwuiztAppService', 'MigratorHelper', 'UserSession', 'WizardService', 'StatService', 'DialogService',
    'ArenaService', 'TwuiztNotifier', 'uiGridConstants',
    function($scope, $rootScope, $filter, $timeout, $interval, $state, $mdMedia, $mdDialog,
        STATE,
        APP_CONFIG, MIGRATOR_EVENTS, MIGRATOR_CONSTANTS,
        MigratorStateManager,
        MigratorLoader,
        TwuiztAppService, MigratorHelper, UserSession, WizardService, StatService, DialogService,
        ArenaService, TwuiztNotifier, uiGridConstants) {
        console.log("Initialized arena controller");

        $scope.$mdMedia = $mdMedia;
        $scope.letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $scope.teamColor = "lightgrey";

        var currentState = {};
        var timeOutColor;
        var timer;
        currentState.name = STATE.stateName;
        currentState.title = {
            alias: STATE.pageName,
            main: STATE.pageName,
            detail: null,
            stateName: STATE.stateName,
            tooltip: null,
            mainClass: null
        };
        $scope.odometerOptions = {
            value: 0,
            auto: true, // Don't automatically initialize everything with class 'odometer'
            selector: '.my-numbers', // Change the selector used to automatically find things to be animated
            format: '(,ddd)', // Change how digit groups are formatted, and how many digits are shown after the decimal point
            // format: '( ddd).dd', // Change how digit groups are formatted, and how many digits are shown after the decimal point
            duration: 3000, // Change how long the javascript expects the CSS animation to take
            theme: 'train-station', // Specify the theme (if you have more than one theme css file on the page)
            animation: 'count' // Count is a simpler animation method which just increments the value,
                // use it when you're looking for something more subtle.
        };
        $scope.currentSession = UserSession.getSession();
        $scope.statusColors = {
            "NEW": "white",
            "PRESENTED": "rgba(255, 152, 0, 0.8)",
            "ACTIVATED": "rgba(3, 169, 244, 0.65)",
            "DONE": "rgba(11, 239, 58, 0.63)",
            "DISCARDED": "lightgrey"
        };
        $scope.odometerOptions = {
            value: 0,
            auto: true, // Don't automatically initialize everything with class 'odometer'
            // selector: '.my-numbers', // Change the selector used to automatically find things to be animated
            format: '(,ddd)', // Change how digit groups are formatted, and how many digits are shown after the decimal point
            // format: '( ddd).dd', // Change how digit groups are formatted, and how many digits are shown after the decimal point
            duration: 1000, // Change how long the javascript expects the CSS animation to take
            theme: 'train-station', // Specify the theme (if you have more than one theme css file on the page)
            animation: 'count' // Count is a simpler animation method which just increments the value,
                // use it when you're looking for something more subtle.
        };

        $scope.gridOptions = { 
            enableFiltering: true,
            onRegisterApi: function(gridApi){
              $scope.gridApi = gridApi;
              // $scope.gridApi.grid.api.core.refresh();
              $scope.gridApi.grid.refresh();
            },
            columnDefs: [
                { field: 'Name' },
                { field: 'Email'},
                { field: 'Pattern'},
                { field: 'Date'}
            ],
            data: [] 
        };

        currentState.actionIcons = STATE.actionIcons;
        MigratorStateManager.setCurrentState(currentState);
        $scope.adminSettings = {
            blacklistedPatterns:[],
            buttonMatrix:{
                rows:null,
                columns:null
            }
        };
        //Initialize the buttons
        $scope.buttonList = [];
        $scope.userInputState = {
            buttonSequence:[],
            detectionList:[]
        };
        $scope.onButtonClick = function(button){
            $scope.userInputState.buttonSequence.push(button.label);
            validateButtonClick(button.label);
        };

        $scope.threatList = [];

        function validateButtonClick(number){

            var blacklistedPatterns = $scope.adminSettings.blacklistedPatterns;
            var detectionList = $scope.userInputState.detectionList;
            var threatDetected = false;
            var i, patternLength;
            var buttonSequence = $scope.userInputState.buttonSequence;
            var maxPatternLength = $scope.adminSettings.maxPatternLength;
            var sequenceCapture;
            for(i =0; i<blacklistedPatterns.length; i++){
                sequenceCapture = buttonSequence.slice(buttonSequence.length-blacklistedPatterns[i].patternLength, buttonSequence.length);
                if(sequenceCapture.toString().indexOf(blacklistedPatterns[i].pattern) == 0){
                    threatDetected = true;
                    detectionList.push(angular.copy(blacklistedPatterns[i]));
                }
            }

            if(threatDetected == true){
                var message = "Unusual activity has been detected of ";
                if(detectionList.length > 1){
                    message+="the following patterns : ";
                }
                else{
                    message+="the pattern : ";
                }
                for(i=0;i<detectionList.length;i++){
                    if(i>0){
                        message+=", ";
                    }
                    message+=detectionList[i].pattern;
                }
                message+=". Acknowledge the alert to continue!";
                var payload = angular.copy(detectionList);
                detectionList.length=0;
                WizardService.registerThreat(payload)
                .then(function(response){
                    TwuiztNotifier.showInfo("Threat has been logged!");
                    processStatData(response.data);
                });
                DialogService.showAlert(null, "THREAT DETECTED!", message)
                .then(function(){
                    console.log("Threat acknowldged!");
                });
            }
        }

        $scope.showButtonSettings = function(actionEvent)
        {
            var templateUrl = "app/modules/arena/button-settings.tmpl.html";
            var controllerName = "AdminButtonSettingsController";
            DialogService.showSettingsDialog(actionEvent, templateUrl, controllerName, {SETTINGS:$scope.adminSettings});
        };

        $scope.editPattern = function(actionEvent, action, pattern)
        {
            console.error("Editing pattern : ",pattern);
            var templateUrl = "app/modules/arena/button-sequence.tmpl.html";
            var controllerName = "AdminButtonSequenceController";
            DialogService.showSettingsDialog(actionEvent, templateUrl, controllerName, {SETTINGS:{action:'edit', pattern:pattern, adminSettings:$scope.adminSettings}});
        };

        $scope.addPattern = function(actionEvent, action, pattern)
        {
            console.error("Editing pattern : ",pattern);
            var templateUrl = "app/modules/arena/button-sequence.tmpl.html";
            var controllerName = "AdminButtonSequenceController";
            DialogService.showSettingsDialog(actionEvent, templateUrl, controllerName, {SETTINGS:{action:'add', pattern:null, adminSettings:$scope.adminSettings}});
        };


        $scope.deletePattern = function(actionEvent, action, pattern){
            var params = {
                id:pattern.id
            };

            DialogService.showConfirmation(actionEvent, "ALERT!", "Delete the selected pattern? "+pattern.pattern, "CANCEL", "DELETE")
            .then(function(){
                console.log("The user opted to delete the pattern.");
                var payload = {
                    action:action,
                    data: pattern
                };

                WizardService.deletePattern(payload)
                .then(function(response){
                    console.log("Pattern delete successful");
                    TwuiztNotifier.showInfo("Pattern has been successfully deleted.");
                    $mdDialog.hide();
                },
                function(response){
                    console.error("Pattern delete failed.");
                    // $mdDialog.cancel();
                });   

            },
            function(){
                console.log("The user opted not to delete the team.");
            });
        };

        function processStatData(data) {
            var i;
            //update the current session variable on each refresh event
            $scope.currentSession = UserSession.getSession();
            //update the dashboard widgets

            $scope.adminSettings.buttonMatrix.columns = data.buttonMatrix.columns;
            $scope.adminSettings.buttonMatrix.rows = data.buttonMatrix.rows;
            $scope.adminSettings.blacklistedPatterns = data.blacklistedPatterns;
            var maxPatternLength = 0, patternLength;
            for(i=0;i<data.blacklistedPatterns.length;i++){
                patternLength = data.blacklistedPatterns[i].pattern.split(",").length;
                if(maxPatternLength < patternLength){
                   maxPatternLength = patternLength;
                }
            }
            $scope.adminSettings.maxPatternLength = maxPatternLength;
            $scope.buttonList = [];
            var buttonCount = 1;
            for(i=0; i<$scope.adminSettings.buttonMatrix.rows; i++){
                var row = [];
                for(var j=0; j<$scope.adminSettings.buttonMatrix.columns; j++){
                    row.push({label:buttonCount++});
                }
                $scope.buttonList.push(row);
            }

            if($scope.currentSession.user.isAdmin == false){
                if(data.currentUserStats){
                    $scope.threatList = data.currentUserStats.threats.reverse();
                    var date;
                    for(var i =0; i<$scope.threatList.length;i++){
                        date = new Date($scope.threatList[i].ts);
                        $scope.threatList[i].date = date.toLocaleDateString() +" "+ date.toLocaleTimeString();
                    }
                }
            }
            else{
                $scope.threatList = [];
                var date, threatList = [];
                for(var i in data.userStats){
                    for(var j=0 ;j<data.userStats[i].threats.length;j++){
                        date = new Date(data.userStats[i].threats[j].ts);

                        threatList.push({
                            Name:data.userInfo[i].userName,
                            Email:data.userInfo[i].email,
                            Pattern:data.userStats[i].threats[j].pattern,
                            Date: date.toLocaleDateString() +" "+ date.toLocaleTimeString()
                        });
                    }
                }
                $scope.threatList = threatList;
                $scope.gridOptions.data =  $scope.threatList;
                // $scope.gridApi.grid.api.core.refresh();
                $scope.gridApi.grid.refresh();
            }

        }


        $scope.$on('STAT_RELOAD_EVENT', function(event, data) {
            console.info("caught STAT_RELOAD_EVENT :: ", data);
            processStatData(data);
        });


        StatService.getAllStats()
            .then(
                function(response) {
                    console.log("Stats response : " + JSON.stringify(response));
                    processStatData(response);
                },
                function(response) {});
    }
]);
