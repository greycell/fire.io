'use strict';
angular.module('twuizt.arena').
controller('AdminButtonSettingsController', 
    ['$scope', '$mdDialog', '$mdMedia', '$interval',
    'TwuiztNotifier', 'WizardService', 'SETTINGS',
    function($scope, $mdDialog, $mdMedia, $interval,
    TwuiztNotifier, WizardService, SETTINGS) {
    console.log("Preparing to initialize the buttons settings controller.");

    $scope.$mdMedia = $mdMedia;

    $scope.buttonSettings = {
        rows : angular.copy(SETTINGS.buttonMatrix.rows),
        columns : angular.copy(SETTINGS.buttonMatrix.columns)
    };

    $scope.cancelButtonSettings = function(){
        console.log("cancelling button settings");
        $mdDialog.hide();
    };

    $scope.saveButtonSettings = function(){
        console.log("saving button settings");
        //Send request to edit the team.
        var params = {
            columns:$scope.buttonSettings.columns,
            rows:$scope.buttonSettings.rows
        };

        WizardService.updateSettings(params)
        .then(function(response){
            console.log("Button config update successful");
            TwuiztNotifier.showInfo("Button Matrix settings has been successfully updated.");
            $mdDialog.hide();
        },
        function(response){
            console.error("Team create failed.");
            // $mdDialog.cancel();
        });   
    }
}]);

