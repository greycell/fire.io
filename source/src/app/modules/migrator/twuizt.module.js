'use strict';

angular.module('twuizt', [
    // inject app constants first so it can be used in all modules
    'appConfig',

    // inject angular modules
    'ngAnimate', 'ngCookies', 'ngSanitize', 'ngMessages', 'ngMaterial', 'ngStorage',

    // inject extra 3rd party angular modules
    'ui.router', 'angularMoment', 'btford.socket-io', 'countUpModule', 'ngFx',

    // inject our own migrator modules
    'twuizt.arena', 'twuizt.login'
])
/**
 *  SETUP STATE CONFIGURATION FOR Twuizt MODULE
 */
.config([ '$stateProvider', '$urlRouterProvider', 'APP_CONFIG', function ($stateProvider, $urlRouterProvider, APP_CONFIG) {

    // setup public states & routes
    $stateProvider
    .state(APP_CONFIG.states.redirect, {
        abstract: false,
        url: '/redirect',
        resolve:{
            USER_SESSION: ['UserSession', '$q', '$state', '$interval', 'SessionManager', function(UserSession, $q, $state, $interval, SessionManager)
                {
                    var deferred = $q.defer();
                    console.info(APP_CONFIG.states.redirect+":: Attempting to resolve USER SESSION.");
                    UserSession.isUserAuthenticated()
                    .then(
                        function(response)
                        {
                            console.log(APP_CONFIG.states.baseAbstract+":: User is logged in : ",response);
                            var toStateObj = SessionManager.getToStateObject();
                            console.log("Attempting to re-direct the user to : "+toStateObj.name);
                            if($state.current.name != toStateObj.name)
                            {
                                $state.go(toStateObj.name, toStateObj.params);
                            }
                            else
                            {
                                deferred.reject(response);
                            }
                        },
                        function(response)
                        {
                            console.log(APP_CONFIG.states.redirect+":: User is not logged in : ",response);
                            console.log("Attempting to re-direct the user to the login page. ");
                            if($state.current.name == APP_CONFIG.states.login)
                            {
                                // $state.go(APP_CONFIG.states.login);
                                deferred.reject(response);
                            }
                        });

                    return deferred.promise;
                }]
        }
    })
    .state(APP_CONFIG.states.baseAbstract, {
        abstract: false,
        url: '/twuizt',
        templateUrl: 'app/modules/migrator/base-layouts/admin-panel/admin-panel.tmpl.html',
        resolve:{
            USER_SESSION: ['UserSession', '$q', '$state', '$interval', function(UserSession, $q, $state, $interval)
                {
                    var deferred = $q.defer();
                    console.info(APP_CONFIG.states.baseAbstract+":: Attempting to resolve USER SESSION.");
                    UserSession.isUserAuthenticated()
                    .then(
                        function(response)
                        {
                            console.log(APP_CONFIG.states.baseAbstract+":: User is logged in : ",response);
                            deferred.resolve(response);
                        },
                        function(response)
                        {
                            console.log(APP_CONFIG.states.baseAbstract+":: User is not logged in : ",response);
                            if($state.current.name == APP_CONFIG.states.login)
                            {
                                // $state.go(APP_CONFIG.states.login);
                                deferred.reject(response);
                            }
                        });

                    return deferred.promise;
                }],
            SYNC_SERVER_TIME: ['TwuiztAppService', function(TwuiztAppService)
                {
                    return TwuiztAppService.getServerTime();
                }]
        },
        data: {
            toolbar: {
                extraClass: '',
                background: false,
                shrink: true
            },
        }
    })
    .state(APP_CONFIG.states.base, {
        abstract: true,
        resolve:{
            ALL_DATA_RESOLVED: ['USER_SESSION', 'SYNC_SERVER_TIME', function(USER_SESSION, SYNC_SERVER_TIME)
            {
                console.log(APP_CONFIG.states.base+":: User session is resolved to load.");
                // return;
            }]
        },
        views: {
            sidebarLeft: {
                templateUrl: 'app/components/sidebar-left/sidebar-left.tmpl.html',
                controller: 'SidebarLeftController'
            },
            sidebarRight: {
                templateUrl: 'app/components/sidebar-right/sidebar-right.tmpl.html',
                controller: 'SidebarRightController'
            },
            toolbar: {
                templateUrl: 'app/components/toolbars/default.tmpl.html',
                controller: 'DefaultToolbarController'
            },
            content: {
                template: '<div layout-fill admin-panel-content flex ui-view="content"></div>'
            }
        },
    });

    // set default routes when no path specified
    $urlRouterProvider.when('', APP_CONFIG.pages.redirect);
    $urlRouterProvider.when('/', APP_CONFIG.pages.redirect);

    // Always go to default page if requested url is not valid.
    $urlRouterProvider.otherwise(APP_CONFIG.pages.redirect);
}])



/**
 *  Providing an interceptor in to the http provider that will listen to the
 *  response errors and redirect the user to an appropriate page
 */
.config(['$httpProvider', function ($httpProvider) {

    // alternatively, register the interceptor via an anonymous factory
    $httpProvider.interceptors.push(['$q', 'MigratorLoader', 'ResponseService', 'LookupService', 'APP_CONFIG',
        function($q, MigratorLoader, ResponseService, LookupService, APP_CONFIG) {

    function toggleAPILoader(config, action)
    {
        var method;
        if(!config || !(config.method))
        {
            return;
        }
        method = config.method.toUpperCase();
        if(method == "POST" || method == "PUT" || method == "DELETE")
        {
            //This API activates a mask that prevents
            //user from interacting the GUI app.
            MigratorLoader.toggleApiLoader(action);
        }
    }

    return {
                'request': function(config)
                {
                    console.debug("Request : "+config.url, config);
                    config.initial = (new Date()).getTime();
                    toggleAPILoader(config, true);
                    return config;
                },
               'requestError': function(rejection) 
               {
                    console.error("Request failed : "+JSON.stringify(rejection));
                    return $q.reject(rejection);
                },
                'response': function(response)
                {
                    toggleAPILoader(response.config, false);
                    //Process the response
                    ResponseService.processResponse(response);

                    if(response.config.isWebApiRequest == true)
                    {
                        response = response.data;
                    }

                    //Check the flag 'isHttpError' in the processed response to decide
                    //whether or not to reject the response.
                    if(response.isHttpError)
                    {
                        //Reject and return response;
                        return $q.reject(response);
                    }

                    return $q.resolve(response);
                },
                // optional method
               'responseError': function(response)
               {
                    //Toggle the state change loader OFF. It is possible that this web api call was
                    //in the middle of a state change. And hence the loader must be called off, so the user may
                    //continue after this error
                    console.error("responseError Http interceptor :: ",response);
                    toggleAPILoader(response.config, false);
                    // if(response.status == 403)
                    // {   
                    //     var stateService = LookupService.getService('$state');
                    //     if(stateService.current.name != APP_CONFIG.states.login)
                    //     {
                    //         stateService.go(APP_CONFIG.states.login);
                    //     }
                    // }
                    //Process the response
                    ResponseService.processResponse(response);

                    //Whatever may be the case, since the response is an error, reject the current response.
                    return $q.reject(response);
                }
          };
    }]);
}])

.config(['$mdDateLocaleProvider', function($mdDateLocaleProvider) {
$mdDateLocaleProvider.formatDate = function(date) {
    
    if (date == null) {
        //This code is in place to support the date picker
        //to show a blank string if it requests a date formatting with null
        return ''; 
    }
    var dateObj = new Date(date);
    var month = (dateObj.getMonth() * 1 + 1).toString();
    var date = dateObj.getDate().toString();
    if (1 * month < 10) {
        month = '0' + month;
    }
    if (1 * date < 10) {
        date = '0' + date;
    }
    return dateObj.getFullYear() + '/' + month + '/' + date; //'YYYY/MM/DD';
};
$mdDateLocaleProvider.parseDate = function(date) {
var dateObj = new Date(date);
return dateObj;
};
}])


/**

 *  Themes to be used to style the migrator web app
 */
.config(['$mdThemingProvider', 'MigratorThemingProvider', 'MigratorSkinsProvider', 'APP_CONFIG', function ($mdThemingProvider, MigratorThemingProvider, MigratorSkinsProvider, APP_CONFIG) {
    /**
     *  PALETTES
     */
    $mdThemingProvider.definePalette('white', {
        '50': 'ffffff',
        '100': 'ffffff',
        '200': 'ffffff',
        '300': 'ffffff',
        '400': 'ffffff',
        '500': 'ffffff',
        '600': 'ffffff',
        '700': 'ffffff',
        '800': 'ffffff',
        '900': 'ffffff',
        'A100': 'ffffff',
        'A200': 'ffffff',
        'A400': 'ffffff',
        'A700': 'ffffff',
        'contrastDefaultColor': 'dark',    // whether, by default, text (contrast)
    });

    $mdThemingProvider.definePalette('black', {
        '50': 'e1e1e1',
        '100': 'b6b6b6',
        '200': '8c8c8c',
        '300': '646464',
        '400': '4d4d4d',
        '500': '3a3a3a',
        '600': '2f2f2f',
        '700': '232323',
        '800': '1a1a1a',
        '900': '121212',
        'A100': 'ffffff',
        'A200': 'ffffff',
        'A400': 'ffffff',
        'A700': 'ffffff',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
    });

    var miOrangeWarn = $mdThemingProvider.extendPalette('orange', {
    'A700': 'FF9800'
    });
    $mdThemingProvider.definePalette('migWarnOrange', miOrangeWarn);
    /**
     *  SKINS
     */

    // MIGRATOR GREY SKIN
    MigratorThemingProvider.theme('migrator-grey')
    .primaryPalette('light-blue', {
        'default': '700',
        'hue-1': '100',
        'hue-2': '500',
        'hue-3': '700'
    })
    .accentPalette('blue', {
        'default': '500',
        'hue-1': '100',
        'hue-2': '500',
        'hue-3': '900'
    })
    .warnPalette('migWarnOrange', {
        'default': '500',
        'hue-1': 'A100',
        'hue-2': 'A200',
        'hue-3': 'A700'
    });

    MigratorSkinsProvider.skin('migrator-grey', 'Migrator Grey')
    .sidebarTheme('migrator-grey')
    .toolbarTheme('migrator-grey')
    .logoTheme('migrator-grey')
    .contentTheme('migrator-grey');

    $mdThemingProvider.theme('MessagesTheme')
    .primaryPalette('red', {
        'default': '800'
    })
    .accentPalette('grey', {
        'default': '500'
    })
    .warnPalette('migWarnOrange', {
        'default': '500',
        'hue-1': 'A100',
        'hue-2': 'A200',
        'hue-3': 'A700'
    });


    /**
     *  SET DEFAULT SKIN
     */
    // MigratorSkinsProvider.setSkin(APP_CONFIG.defaultSkin);
    MigratorSkinsProvider.setSkin("migrator-grey");

}])
.factory('socket', function (socketFactory) {
    
    return socketFactory;
})
.run(['$rootScope', '$interval', '$window', '$state','MigratorCommService', 'MigratorWidgetService',  'MIGRATOR_EVENTS', 'SessionManager',
    function ($rootScope, $interval, $window, $state, MigratorCommService, MigratorWidgetService, MIGRATOR_EVENTS, SessionManager) {
    console.info("Initiated Migrator state service : "+JSON.stringify(MigratorCommService));
    $window.addEventListener("resize", function() {
        console.error("RESIZE EVENT BROADCAST");
        $interval(function() {
            $rootScope.$broadcast("resize");
        }, 0, 1)
    });

    
}]);