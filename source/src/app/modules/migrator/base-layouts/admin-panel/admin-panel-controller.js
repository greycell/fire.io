'use strict';

/**
 * @ngdoc function
 * @name AdminController
 * @module migrator
 * @kind function
 *
 * @description
 *
 * Handles the admin view
 */

angular.module('twuizt').
controller('AdminController',['$scope', '$rootScope', '$interval', '$state',
    function ($scope, $rootScope, $interval, $state) {
    $scope.toolbarShrink = undefined;
    $scope.isMenuLocked = true; 
    $scope.isMenuCollapsing = false; 
    $scope.isHovering = false;  

    $scope.isUserAuthenticated = $rootScope.gdata;

    if($state.current.data !== undefined) {
        if($state.current.data.toolbar !== undefined) {
            if($state.current.data.toolbar.shrink === true) {
                $scope.toolbarShrink = true;
            }
        }
    }
 

    // we need different event handlers for mouse over / leave, can not toggle the variable.
    $scope.activateHover = function() {      
        $scope.isHovering = true;
    };

    $scope.removeHover = function(){        
        $scope.isHovering = false;
    };

    $scope.toggleMenuLock = function() {    
        $scope.isMenuLocked = !$scope.isMenuLocked;        
        $scope.isMenuCollapsing = !$scope.isMenuLocked;
        
        if($scope.isMenuCollapsing === true){  
            // manually remove the hover class in order to prevent the menu from growing back
            $scope.isHovering = false;            
            $interval(function() {                
                $scope.isMenuCollapsing = false;                  
            }, 400,1);
        }           
    };

    $scope.menuClass = function() {        
        // return  $scope.isMenuLocked === true ? '' :($scope.isMenuCollapsing === true ? 'is-collapsing' : ($scope.isHovering == true ? 'admin-sidebar-collapsed hover': 'admin-sidebar-collapsed' ));
        return  'admin-sidebar-collapsed';
    };

    $(window).resize(function(){
    $scope.$apply(function(){
           debounce(function(){
                        console.log("Broadcasting window resize event.");
                        console.log("Window resize detected. Current Height : "+window.innerHeight+" Current Width : "+window.innerWidth);
                       $rootScope.$broadcast('WindowResizeEvent', null);
                     }, 500)();
    });
});

$scope.lastDebounceCall = -1;

function debounce(func, interval) 
{
    // console.log("debouncing...");
    //$scope.lastDebounceCall = -1;
    return function() {
        clearTimeout($scope.lastDebounceCall);
        var args = arguments;
        var self = this;
        $scope.lastDebounceCall = setTimeout(function() {
            func.apply(self, args);
        }, interval);
    };
}



}]).run([function($rootScope){
     console.log("The run function of the admin controller is activated");
}]);