'use strict';

angular.module('twuizt')
.controller('TwuiztMainController',[ '$rootScope', '$scope', function($rootScope, $scope)
{
    $scope.isBlurActive = false;
    $scope.isMigratorAppLoaded = false;
    $rootScope.$on('ToggleTwuiztInactivitySuspender', function(evt, action, current)
    {
        $scope.isBlurActive = action;

        if(!$scope.$$phase) 
        {
            $scope.$apply(true);
        }        
    });

    var UnregisterFn = $rootScope.$on('$stateChangeSuccess',function(evt, next, current)
    {
        UnregisterFn();
    	$scope.isMigratorAppLoaded = true;
    	if(!$scope.$$phase) 
        {
            $scope.$apply(true);
        }
    });  
}]);