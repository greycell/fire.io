'use strict';

/**
 * @ngdoc function
 * @name LoginController
 * @module migrator.login
 * @kind function
 *
 * @description
 *
 * Handles login form submission and response
 */
angular.module('twuizt.login')
.controller('LoginController', ['$scope', '$state', '$interval', 
  'MIGRATOR_EVENTS', 'APP_CONFIG',
  'MessageService', 'TwuiztNotifier', 'UserSession', 'DialogService', 'SessionManager',
  function ($scope, $state, $interval, 
    MIGRATOR_EVENTS, APP_CONFIG,
    MessageService, TwuiztNotifier, UserSession, DialogService, SessionManager) {

    console.log("Loading the login controller");

    $scope.loginModel = {
      isLoaderActive : true
    };
    $scope.tokenId = null;
    $scope.twuiztGoogleAuthConfig = {
                // 'client_id': "848621533265-n4b2ha7rhlbm1nbm52gue1c4pd2cvoqt.apps.googleusercontent.com", // CLIENT_ID from developer console which has been explained earlier.
                'client_id': "433228304602-qtl7quh464o82o9cnvor86nnt4s7sfa1.apps.googleusercontent.com", // CLIENT_ID from developer console which has been explained earlier.
                'requestvisibleactions': 'http://schemas.google.com/AddActivity', // Visible actions, scope and cookie policy wont be described now,
                'prompt':'select_account',                                                      // as their explanation is available in Google+ API Documentation.
                'scope': 'profile email',
                // 'cookiepolicy': 'single_host_origin',
                "approvalprompt" : "force",
                "authuser": -1,
                'cookiepolicy': 'none',
                'immediate':false
            };
    // document.getElementsByClassName("twuizt-play-button")[0].addEventListener("click", doLogin, true);


    // function doLogin($event){
    //  // GAuth.login($event);
    // };

    $scope.clickHandler = function()
    {
      // $scope.toggleLoader(true);
      // gapi.auth2.init(angular.element("twuizt-google-authentication").scope().twuiztGoogleAuthConfig)
      // gapi.auth2.getAuthInstance().disconnect()
      // .then(function(){
      //   gapi.auth2.authorize($scope.twuiztGoogleAuthConfig, onSignIn, onFailure);
      // });

      gapi.auth2.getAuthInstance().signIn($scope.twuiztGoogleAuthConfig).then(onSignIn, onFailure);

      // $scope.toggleLoader(true);

    }
    
    function onFailure()
    {
      $scope.toggleLoader(false);
    }

    function onSignIn(googleUser){
      $scope.tokenId = googleUser.Zi.access_token;
      console.error(JSON.stringify(googleUser));
      $scope.toggleLoader(true);
      UserSession.authenticate($scope.tokenId)
      .then(
        function(response){
          console.warn(response);
          // UserSession.setUser(gapi.auth2.getAuthInstance().currentUser);
          console.log("User is logged in to google : ",response);
          
          if(response.isApiError)
          {
            if(response.apiStatus == "NOT_AUTHORIZED")
            { 
              $scope.toggleLoader(false);
            }
            DialogService.showApiErrorDialog(null, response.apiStatusMessage);
            return;
          }

          $interval(function(){
            var toStateObj = SessionManager.getToStateObject();
            $state.go(toStateObj.name, toStateObj.params);
          },1,1);

        },
        function(response){
            console.error(response);
            $scope.toggleLoader(false);
        })
    }

    $scope.$on('GoogleApiLoadedEvent', function() {
      console.warn("Googe api is loaded");
      $scope.toggleLoader(false);
    });

    $scope.toggleLoader = function(action)
    {
      $interval(function(){
        $scope.loginModel.isLoaderActive = action;
      },1,1); 
    }

}]);