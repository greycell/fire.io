'use strict';

/**
 * @ngdoc module
 * @name migrator.login
 * @description
 *
 * The `twuizt.login` module adds the login page
 */
angular.module('twuizt.login', [])
.config([ '$stateProvider', 'APP_CONFIG', 
	function ($stateProvider, APP_CONFIG) {

    $stateProvider
    .state(APP_CONFIG.states.login, {
        url: '/login',
        resolve:{
            USER_SESSION: ['UserSession', '$q', '$state', '$interval', 'SessionManager', function(UserSession, $q, $state, $interval, SessionManager)
                {
                    var deferred = $q.defer();
                    console.info("Attempting to resolve USER SESSION.");
                    UserSession.isUserAuthenticated(false)
                    .then(
                        function(response)
                        {
                            console.log(APP_CONFIG.states.login+":: User is logged in : ",response);
                            var toStateObj = SessionManager.getToStateObject();
                            if($state.current.name != toStateObj.name)
                            {
                                $state.go(toStateObj.name, toStateObj.params);
                            }
                            else
                            {
                                deferred.reject(response);
                            }
                            console.log("User is already logged in. Deny state change.");
                        },
                        function(response)
                        {
                            console.log(APP_CONFIG.states.login+":: User is not logged in : ",response);
                            deferred.resolve(response);
                        });

                    return deferred.promise;
                }]
        },
        templateUrl: 'app/modules/login/login.tmpl.html',
        controller: 'LoginController'
    });

}])
.run(function() {
    console.log("Run function of login module.");
});
