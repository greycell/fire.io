angular.module('twuizt')  
.service('WizardService', ['APP_CONFIG', 'HttpService', '$q', function(APP_CONFIG, HttpService, $q) 
{

  var wizardService = {};
  var SETTINGS_URL=APP_CONFIG.baseWebApiUrl+'/admin/settings';
  var MANAGE_QUESTION_URL=APP_CONFIG.baseWebApiUrl+'/admin/question';
  var GET_QUESTIONS_URL=APP_CONFIG.baseWebApiUrl+'/admin/questions';
  var THREAT_URL = APP_CONFIG.baseWebApiUrl+'/threat';
  wizardService.getAllQuestions = function()
  {
    var deferred = $q.defer();
    HttpService.sendHttp({
              method: 'GET',
              url: GET_QUESTIONS_URL
          })
    .then(function(response){
      console.info("Successfully fetched all questions : "+JSON.stringify(response));

      deferred.resolve(response);
    },
      function(response){
      deferred.reject(response);
      });

    return deferred.promise;
  };

  wizardService.manageQuestion = function(action, id)
  {
    var deferred = $q.defer();
    var payload = {
      action:action,
      data:{
         id:id
      }
    };

    HttpService.sendHttp({
              method: 'POST',
              url: MANAGE_QUESTION_URL,
              timeout: 5000,
              data: payload
          })
    .then(function(response){
      console.info("Successfully created a new team : "+JSON.stringify(response));

      deferred.resolve(response);
    },
      function(response){
      deferred.reject(response);
      });

    return deferred.promise;
  };




  wizardService.addTeam = function(params)
  {
    var deferred = $q.defer();
    var payload = {
      action:"add",
      data:{
         name : params.name,
         color : params.color
      }
    };

    HttpService.sendHttp({
              method: 'POST',
              url: SETTINGS_URL,
              timeout: 5000,
              data: payload
          })
    .then(function(response){
      console.info("Successfully created a new team : "+JSON.stringify(response));

      deferred.resolve(response);
    },
      function(response){
      deferred.reject(response);
      });

    return deferred.promise;
  }

    wizardService.updateSettings = function(params)
    {
      var deferred = $q.defer();
      
      var payload = {
        action:"BUTTON_SETTINGS",
        data:params
      };

      HttpService.sendHttp({
                method: 'POST',
                url: SETTINGS_URL,
                timeout: 5000,
                data: payload
            })
      .then(function(response){
        console.info("Successfully edited the settings : "+JSON.stringify(response));

        deferred.resolve(response);
      },
        function(response){
        deferred.reject(response);
        });

      return deferred.promise;
    }

    wizardService.registerThreat = function(payload){
      var deferred = $q.defer();

      HttpService.sendHttp({
                method: 'POST',
                url: THREAT_URL,
                timeout: 5000,
                data: payload
            })
      .then(function(response){
        console.info("Successfully registered the threat : "+JSON.stringify(response));

        deferred.resolve(response);
      },
        function(response){
        deferred.reject(response);
        });

      return deferred.promise; 
    };

    wizardService.updatePattern = function(payload)
    {
      var deferred = $q.defer();

      HttpService.sendHttp({
                method: 'POST',
                url: SETTINGS_URL,
                timeout: 5000,
                data: payload
            })
      .then(function(response){
        console.info("Successfully edited the settings : "+JSON.stringify(response));

        deferred.resolve(response);
      },
        function(response){
        deferred.reject(response);
        });

      return deferred.promise;
    }

    wizardService.deletePattern = function(payload)
    {
      var deferred = $q.defer();

      HttpService.sendHttp({
                method: 'POST',
                url: SETTINGS_URL,
                timeout: 5000,
                data: payload
            })
      .then(function(response){
        console.info("Successfully edited the settings : "+JSON.stringify(response));

        deferred.resolve(response);
      },
        function(response){
        deferred.reject(response);
        });

      return deferred.promise;
    };

    wizardService.addPlayer = function(params)
    {
      var deferred = $q.defer();
      var payload = {
      action:"add_player",
      data:{
         id : params.id,
         userId : params.userId
      }
    };

      HttpService.sendHttp({
                method: 'POST',
                url: SETTINGS_URL,
                timeout: 5000,
                data: payload
            })
      .then(function(response){
        console.info("Successfully added player to team : "+JSON.stringify(response));

        deferred.resolve(response.data);
      },
        function(response){
        deferred.reject(response);
        });

      return deferred.promise;
    }

    wizardService.deletePlayer = function(params)
    {
      var deferred = $q.defer();
      var payload = {
      action:"delete_player",
      data:{
         id : params.id,
         userId : params.userId
      }
    };
      HttpService.sendHttp({
                method: 'POST',
                url: SETTINGS_URL,
                timeout: 5000,
                data: payload
            })
      .then(function(response){
        console.info("Successfully deleted player from team : "+JSON.stringify(response));

        deferred.resolve(response.data);
      },
        function(response){
        deferred.reject(response);
        });

      return deferred.promise;
    }

  return wizardService;

}]);