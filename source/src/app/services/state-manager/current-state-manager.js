'use strict';
//This service must be used to set the current state for a page when it is initialized.
angular.module('twuizt')  
.service('MigratorStateManager', ['$rootScope', '$state', 'APP_CONFIG', 'UserSession', 
  function ($rootScope, $state, APP_CONFIG, UserSession) 
{

  var migratorStateManager = {};

  migratorStateManager.titles = {

  };

  migratorStateManager.states = [];
  
  migratorStateManager.setCurrentState = function(currentState)
  {

    glorifyActionIcons(currentState.actionIcons);

    var actionIconsForCurrentState = {
      name : currentState.name
    };
    var isIconSetAvailable = false;

    console.info("State set for : "+JSON.stringify(currentState));
    console.info("Setting the title for the current page : "+currentState.name);
    migratorStateManager.titles[currentState.name] = currentState.title;

    actionIconsForCurrentState.actionIcons = currentState.actionIcons;

    for(var i in migratorStateManager.states)
    {
      // console.error("Is '"+migratorStateManager.states[i].name+"' == '"+currentState.name+"'");
      if(migratorStateManager.states[i].name == currentState.name)
      {
        isIconSetAvailable = true;
        // console.error("icon state is already available. Replacing.");
        migratorStateManager.states[i].actionIcons = currentState.actionIcons;
        break;
      }
    }

    if(!isIconSetAvailable)
    {
      // console.error("Adding new icon states.");
      migratorStateManager.states.push(actionIconsForCurrentState);
    }
    // console.error("Titles : "+JSON.stringify(migratorStateManager.titles));
    // console.error("Action Icons : "+JSON.stringify(migratorStateManager.states));
  };

  migratorStateManager.getIconState = function(stateName, action_button)
  {
    console.log("Attempting to fetch action button : "+action_button+" from state : "+stateName);
    for(var i in migratorStateManager.states)
    {
      console.log("action button : "+stateName +" == " + migratorStateManager.states[i].name);
      if(stateName == migratorStateManager.states[i].name)
      {
        console.log("action button : Found the state : "+migratorStateManager.states[i].name);
        for(var j in migratorStateManager.states[i].actionIcons)
        {
          console.log("action button : "+migratorStateManager.states[i].actionIcons[j].action +" == "+ action_button);
          if(migratorStateManager.states[i].actionIcons[j].action == action_button)
          {
            console.log("Returning action button obj : "+JSON.stringify(migratorStateManager.states[i].actionIcons[j]));
            return migratorStateManager.states[i].actionIcons[j];
          }
        }
        break;
      }
    }
    console.log("action button is not found.");
    return null;
  };


  function glorifyActionIcons(actionIcons)
  {
    for(var i in actionIcons)
    {

      actionIcons[i].activate = function()
      {
        console.log("Activating action_button.");
        this.isActivated = true;
      };

      actionIcons[i].deactivate = function()
      {
        console.log("deactivating action_button.");
        this.isActivated = false;
      };

      actionIcons[i].show = function()
      {
        console.log("Show action_button.");
        this.hidden = false;
      };

      actionIcons[i].hide = function()
      {
        console.log("Hide action_button.");
        this.hidden = true;
      };
    }
    // console.error("Glorified action icons : "+JSON.stringify(actionIcons));
  }
  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams)
  {
      var result = migratorStateManager.allowUserAccess(APP_CONFIG.rolesAlllowedForStates[toState.name],UserSession.getUser().roles);
      console.error("State change start - ",toState);
      if(result != null && result == false)
      {
        console.error("User does not have the roles to access this page preventDefault");  
        event.preventDefault();
        $state.go(APP_CONFIG.states.defaultState);
      }  
  });

   migratorStateManager.allowUserAccess = function(allowedRoles,currentRoles)
  {
    //Compare the roles defined for state with the current user
    //roles, If user has any of the roles specified in the state
    //access property then display the menu item otherwise not.
    if(allowedRoles != null && currentRoles != null)
    {
      for(var i=0;i<allowedRoles.length;i++)
      {
          for(var j=0;j<currentRoles.length;j++)
          {
                if(allowedRoles[i] == currentRoles[j])
                {
                  return true;
                }

          }
      }
      return false;    
    }
    else
    {
      return null;
    }  

  }

  return migratorStateManager;

}]);