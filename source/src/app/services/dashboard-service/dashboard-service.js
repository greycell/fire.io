angular.module('twuizt')  
.service('LeaderboardService', ['APP_CONFIG', 'HttpService', '$q', function(APP_CONFIG, HttpService, $q) 
{

  var leaderboardService = this;
  
  leaderboardService.getServerTime = function()
  {
    unique_token = null;
    var deferred = $q.defer();
    var URL=APP_CONFIG.baseWebApiUrl+'/getSession';

    HttpService.sendHttp({
              method: 'GET',
              url: URL,
              timeout: 5000
          })
    .then(function(response){
      console.log("The server time is : "+JSON.stringify(response));
      deferred.resolve(response);
    },
      function(response){
      deferred.reject(response);
      });

    return deferred.promise;
  }

  return leaderboardService;

}]);