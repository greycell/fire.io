'use strict';
angular.module('twuizt')  
.service('DialogueInternalHelper', [function(){
  var dialogueInternalHelper = {};

  dialogueInternalHelper.isActiveDialogueResolved = function(promise)
  {
    if(promise == null)
    {
      return true;
    }

    if(promise.$$state.status == 0)
    {
      return false;
    }

    return true;
  };

	return dialogueInternalHelper;
}]);