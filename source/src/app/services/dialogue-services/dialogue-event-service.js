'use strict';
angular.module('twuizt')
.service('DialogueEventService', ['$rootScope', 
	function($rootScope){
	var dialogueEventService = {};

  dialogueEventService.sendHttpErrorEventForDialog = function(messageObject)
  {
    console.log("sendHttpErrorEventForDialog service request came in with : ",messageObject);
    $rootScope.$broadcast('HttpErrorEvent', messageObject);
  };

	return dialogueEventService;
}]);
