'use strict';
angular.module('twuizt')
.service('DialogService', ['$rootScope', '$mdDialog', '$mdMedia', '$injector', '$state',
  'MIGRATOR_CONSTANTS',
  'MessageService', 'DialogueInternalHelper', 
  function($rootScope, $mdDialog, $mdMedia, $injector, $state,
    MIGRATOR_CONSTANTS,
    MessageService, DialogueInternalHelper){

  var customFullscreen = $mdMedia('xs') || $mdMedia('sm');	
  var unsavedChanges = {
            title : MessageService.getMessageString("TITLE", MIGRATOR_CONSTANTS.COMMON.UNSAVED_DIALOG_CONSTANTS),
            body : MessageService.getMessageString("BODY", MIGRATOR_CONSTANTS.COMMON.UNSAVED_DIALOG_CONSTANTS),
            primaryAction : MessageService.getMessageString("PRIMARY", MIGRATOR_CONSTANTS.COMMON.UNSAVED_DIALOG_CONSTANTS),
            secondaryAction : MessageService.getMessageString("SECONDARY", MIGRATOR_CONSTANTS.COMMON.UNSAVED_DIALOG_CONSTANTS)
  };
  
  var dialogService = this;
  var confirmationDialogTemplate = '<md-dialog ng-cloak ng-model="dialogue" md-theme="migrator-grey" aria-label="{{dialogue.title}}"><md-dialog-content><div class="md-dialog-content"><h2 class="md-title">{{dialogue.title}}</h2><p>{{dialogue.content}}</p></div></md-dialog-content><md-dialog-actions layout="row"><md-button ng-click="primaryAction()" md-autofocus>{{dialogue.primaryAction}}</md-button><md-button ng-click="secondaryAction()"  style="margin-right:20px;">{{dialogue.secondaryAction}}</md-button></md-dialog-actions></md-dialog>';
  var MigratorWidgetService = null;
  var confirmationDialogController = function($scope, $mdDialog, content, title, primaryAction, secondaryAction){
            $scope.dialogue = {
              title:title,
              content:content,
              primaryAction:primaryAction,
              secondaryAction:secondaryAction
            };
            $scope.secondaryAction = function() {
              $mdDialog.hide();
            };
            $scope.primaryAction = function() {
              $mdDialog.cancel();
            };
          };

  function getMigratorWidgetService(){

    if(MigratorWidgetService == null){
      MigratorWidgetService = $injector.get("MigratorWidgetService");
    }
    return MigratorWidgetService;
  }

  var activeDialogueObject = {
    promise : null
  };

  var defaultCredentialsDialogObject = {
    promise : null
  };
  //This listeners is used to listen and send an event to display http errors
	$rootScope.$on('HttpErrorEvent', function(event, messageObject){
  		return showAlert(event, messageObject.title, messageObject.message, "CLOSE");
	});

  //This API is exposed to all controllers to alert the user of an error.
  //The error dialog will not have a title for an API level error
  // as mentioned in the specification
  dialogService.showApiErrorDialog = function(event, message){
    console.log("Dialogue API error alert called with message : "+message);
    return showAlert(event, "", message, "CLOSE");
  }

	//This is the internal function that will called by 
	// all the api(s) exposed by the dialogue service to alert the user about an ERROR.
	function showAlert(event, title, message, OK_STRING){
    if(DialogueInternalHelper.isActiveDialogueResolved(activeDialogueObject.promise) == false){
      //If any other dialogue is active, return the active dialogue's promise.
      console.warn("Another dialogue instance in active. Ignoring current request for :: ",title, message);
      return activeDialogueObject.promise;
    }
		
    //Close all widgets that are overlayed on the view before calling the error dialogue
    getMigratorWidgetService().closeAllActiveWidgets();

    activeDialogueObject.promise = $mdDialog.show($mdDialog.alert()
                                    .parent(angular.element("body"))
                                    .clickOutsideToClose(true)
                                    .title(title)
                                    .textContent(message)
                                    .ariaLabel(title)
                                    .ok(OK_STRING)
                                    .targetEvent(null)
                                  );

    return activeDialogueObject.promise;
	};

  //force closing the active dialogue
  dialogService.forceCloseActiveDialogue = function(){
    try{
      $mdDialog.cancel();
    }
    catch(e){
      console.error("Unknown error while force closing active dialogues :: ",e);
    }
  };

  dialogService.showAlert = function(event, title, message){
    // Appending dialog to document.body to cover sidenav in docs app
    // Modal dialogs should fully cover application
    // to prevent interaction outside of dialog
    return $mdDialog.show(
      $mdDialog.alert()
        .parent(angular.element(document.body))
        .clickOutsideToClose(false)
        .title(title)
        .textContent(message)
        .ariaLabel(title)
        .ok('ACKNOWLEDGE')
        .targetEvent(event)
    );
  };

	//This dialogue is used to prompt the user for confirmation especially in cases like leaving a page without 
	//saving changes
	dialogService.showConfirmation = function(event, title, message, primaryAction, secondaryAction){
		

		console.log("Dialogue confirmation called with title : "+title+" and message : "+message);
		return $mdDialog.show({
          controller: confirmationDialogController,
          template:confirmationDialogTemplate,
          parent: angular.element(document.body),
          targetEvent: event,
          clickOutsideToClose:false,
          fullscreen: false,
          locals:{
          	content:message,
          	title:title,
          	primaryAction:primaryAction,
          	secondaryAction:secondaryAction
          }
        });
  	};
    dialogService.showSettingsDialog = function(event, templateUrl, controllerName, scopeObject){

    if(DialogueInternalHelper.isActiveDialogueResolved(defaultCredentialsDialogObject.promise) == false){
      console.log("There is an already active dialogue instance. Ignoring current request.");
      return;
    }

    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && customFullscreen;
    
    defaultCredentialsDialogObject.promise = $mdDialog.show({
          controller : controllerName,
          templateUrl :templateUrl,
          parent : angular.element(document.body),
          targetEvent : event,
          clickOutsideToClose :false,
          fullscreen : true,
          locals : scopeObject
        });

    return defaultCredentialsDialogObject.promise;
  };

  dialogService.showUnsavedChangesDialog = function (event, toState, toParams, unsavedChangesState){

      dialogService.showConfirmation(event, unsavedChanges.title, unsavedChanges.body, 
        unsavedChanges.primaryAction, unsavedChanges.secondaryAction)
      .then(
        function() {                      
            console.log("Allow user to leave page.");
            unsavedChangesState.checkEnabled = false;
            $state.go(toState, toParams);
        },
        function()  {
          console.log("User chose to stay on current page.");
        });

  };

}]);
