'use strict';
angular.module('twuizt')
.service('ContextMenuService', ['$interval', '$compile', '$mdMenu',
	function($interval, $compile, $mdMenu)
{
	var contextMenuService = this;

    contextMenuService.getContextMenuItemModel = function(id, name)
    {
        return {"id":id, "name":name};
    };

    contextMenuService.getContextMenu = function(currentScope, modelName,  positionMode, offsets, onContextMenuItemClickCallback, onContextMenuCloseCallback)
    {
        console.log("Preparing to compile the context menu.");
        var contextMenu = angular.element('<div ng-model="'+modelName+'" class="md-open-menu-container md-whiteframe-z2"><md-menu-content width="4" class="context-menu-selector-class"><md-menu-item ng-repeat="action in  '+modelName+'.actions | orderBy:action.id "><md-button ng-click="_onContextMenuItemClickCallback($event, '+modelName+'.entityId, action) ">{{action.name}}</md-button></md-menu-item></md-menu-content></div>');

        currentScope._onContextMenuItemClickCallback = onContextMenuItemClickCallback;
        
        $compile(contextMenu.contents())(currentScope);

        var contextMenuControl = {

          open: function(target) {
             this.currentTarget = target;

             $mdMenu.show({
                  scope: currentScope,
                  mdMenuCtrl: contextMenuControl,
                  element: contextMenu,
                  target:target
                });

            },
            close: function() {
                $mdMenu.hide();
                var targetToCollapse = this.currentTarget;
                $interval(function(){
                    if(onContextMenuCloseCallback != null && onContextMenuCloseCallback.constructor == Function)
                    {
                        onContextMenuCloseCallback();
                    }
                },200,1);
             },
            positionMode: function() { return positionMode; },
            offsets: function() { return offsets; },
            currentTarget:null
        };

        console.log("Context menu initialized : ", contextMenuControl);
        return contextMenuControl;
    };

}]);