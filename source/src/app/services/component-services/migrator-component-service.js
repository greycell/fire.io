'use strict';
angular.module('twuizt')
.service('MigratorWidgetService', ['$rootScope', '$mdSelect', '$mdMenu', 'DialogService', 
  function($rootScope, $mdSelect, $mdMenu, DialogService)
{
	var migratorWidgetService = {};

   migratorWidgetService.closeSelectWidget = function()
  {
     console.error("Closing active select widgets");
     $mdSelect.hide();
  };

  migratorWidgetService.closeMenuWidget = function()
  {
     console.error("Closing active menu widgets");
     $mdMenu.hide();
  };

  migratorWidgetService.closeDialogWidget = function()
  {
     console.error("Closing active dialogues");
     DialogService.forceCloseActiveDialogue();
  };


  migratorWidgetService.closeCalendarWidget = function()
  {
     console.error("Closing active calendar controls");
    $rootScope.$broadcast("md-calendar-close");
  };

  migratorWidgetService.closeAllActiveWidgets = function()
  {
    migratorWidgetService.closeCalendarWidget();
    migratorWidgetService.closeDialogWidget();
    migratorWidgetService.closeMenuWidget();
    migratorWidgetService.closeSelectWidget();
  }

	return migratorWidgetService;
}]);