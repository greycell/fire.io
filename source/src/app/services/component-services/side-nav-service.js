'use strict';
angular.module('twuizt')
.service('SideNavService', ['$rootScope', '$mdSidenav', '$mdUtil', '$document', '$q', 
	function($rootScope, $mdSidenav, $mdUtil, $document, $q)
{
	var sideNavService = this;

	sideNavService.handleRightSideMenuOpenEvent = function(activePanel, requestedPanel)
    {
    	var deferred = $q.defer();
        console.log("Current active panel: "+activePanel);
        console.log("requestedPanel: "+requestedPanel);
        $document[0].activeElement.blur();
        console.log("Checking whether other menus are active.");
        if($mdSidenav("rightSideBar").isOpen())
        {
            console.log("The right menu is already open.");

            $mdSidenav("rightSideBar")
                  .close()
                  .then(function ()
                      {
                        if(activePanel == requestedPanel || requestedPanel == null)
                        {
                            console.log("Migrator right side nav bar closed.");
                            deferred.reject(requestedPanel);
                            return;
                        }

            			deferred.resolve(requestedPanel);
                        
                        $mdUtil.debounce(function(){
                            $mdSidenav("rightSideBar").open();
                         }, 300)();
                      }
                  );
        }
        else
        {
            if(requestedPanel == null)
            {
                deferred.reject(null);
                return;
            }
            console.log("The right menu is not active");
            deferred.resolve(requestedPanel);
            $mdUtil.debounce(function(){
                    $mdSidenav("rightSideBar").open();
                 }, 220)();
        }
        return deferred.promise;
    };
}]);