'use strict';
angular.module('twuizt')
.service('ResponseService', ['$location',  
	'APP_CONFIG', 'HTTP_ERRORS',
	'DialogueEventService', 'MessageService', 'LookupService', 
	function($location,
	APP_CONFIG, HTTP_ERRORS,
	DialogueEventService, MessageService, LookupService)
{
	var responseService = this;

	var stateRouterService = null;
	var sessionManagerService = null;
	var migratorWidgetService = null;
	var intervalService = null;

	function getMigratorWidgetService()
	{
		if(migratorWidgetService == null)
		{
			migratorWidgetService = LookupService.getService('MigratorWidgetService');
		}

		return migratorWidgetService;
	}

	function getStateRouterObject()
	{
		if(stateRouterService == null)
		{
			stateRouterService = LookupService.getService('$state');
		}

		return stateRouterService;
	}

	function getSessionManagerService()
	{
		if(sessionManagerService == null)
		{
			sessionManagerService = LookupService.getService('SessionManager');
		}

		return sessionManagerService;
	}

	function getIntervalService()
	{
		if(intervalService == null)
		{
			intervalService = LookupService.getService('$interval');
		}

		return intervalService;
	}

	responseService.processResponse = function(response)
	{
		console.info("Pre-processing response - http interceptor");
		var httpStatusCode = response.status;
		var isHttpError = true;
		var isApiError = true;
		var migratorApiStatusCode = null;
		var apiStatusCodesObj = null;
		var statusMessage = null;
	
		//Check and verify whether the request is an http error
		isHttpError = isRequestHttpError(response);

		if(isHttpError)
		{
			//Check for the flag set by the http wrapper service
			//to differentiate between web api requests and requests made by
			//the angular framework.	
			console.error("Request flagged with an http error condition.");
			if(response.config.isWebApiRequest == true)
			{
				//Glorify the response only if it a web api request
				glorifyResponse(response, response.statusMessage, isHttpError, isApiError);
			}
			return;	
		}

		//If it is not an http error, return if its not a web api request
		if(response.config.isWebApiRequest != true)
		{
			//If the request is not that of a webapi, return.
			return;
		}

		//If the request is a webapi request and is not an http error, 
		//validate its API status code
		migratorApiStatusCode = response.data.apiStatus;
		// apiStatusCodesObj = response.config.statusCodes;
		// if(isNotDefined(migratorApiStatusCode))
		// {
		// 	console.error("Unable to process the response string because this statusCode is not defined in the constants.");
		// 	isHttpError = true;
		// 	isApiError = true;
		// 	glorifyResponse(response, statusMessage, isHttpError, isApiError);
		// 	showUnknownStatusCodeError();
		// 	return;
		// }

		// migratorApiStatusCode = migratorApiStatusCode.toString().toUpperCase();

		//Verify whether API status code is an internal error
		if(isInternalError(migratorApiStatusCode))
		{
			isHttpError = true;
			isApiError = true;
			glorifyResponse(response, statusMessage, isHttpError, isApiError);

			//if the migrator status code is an internal error,
			//we treat it exactly like the http internal error. 
			//But we call this internal error internally as pseudo-internal error
			handleHttpError(migratorApiStatusCode);
			return;
		}

		//Access the mapped message for the current API status code
		// statusMessage = apiStatusCodesObj[migratorApiStatusCode];
		//If the status message is undefined, notify the user of the undefined API status code
		// if(isNotDefined(statusMessage))
		// {
		// 	isHttpError = true;
		// 	isApiError = true;
		// 	glorifyResponse(response, statusMessage, isHttpError, isApiError);
		// 	showUnknownStatusCodeError();
		// 	return;
		// }

		//If the API status matches the configured success string, 
		//the API call can be considered a success. Mark the flag appropriately and proceed.
		if(migratorApiStatusCode.toString().toUpperCase() != "SUCCESS")
		{
			isApiError = true;	
		}
		else 
		{
			//If the migratorApiStatusCode does not match the configured SUCCESS string,
			//it can be considered as an API error condition. If so, mark the flag appropriately and proceed.
			isApiError = false;
		}
		
		//Glorify the response.data, so the controllers may use the glorified properties to 
		//handle the situations appropriately.

		//If there are any messageParams, they need to be replaced in the message string
		// if(response.config.messageParams != null)
		// {
		// 	statusMessage = MessageService.getMessageString(migratorApiStatusCode, apiStatusCodesObj, response.config.messageParams);
		// }
		glorifyResponse(response, statusMessage, isHttpError, isApiError);
	};

	function isRequestHttpError(response)
	{
		var httpStatusCode = response.status;
		var delayToProcess = 0;
		var showError = true;
		var service;
		//If the server returned null as request status code
		if(isNotDefined(httpStatusCode))
		{
			isHttpError = true;
			isApiError = true;
			showUnknownStatusCodeError();
			return true;
		}

		//Check if the response was a 401, if it is, redirect the user to the login page.
		if(httpStatusCode == 401)
		{
			// getSessionManagerService().destroyUserSession();
			getMigratorWidgetService().closeAllActiveWidgets();
			console.error("Redirect user to login state :: "+response.config.redirect);
			//Inject the state variable on demand and redirect the user to the login page.
			//Do not route to authentication page, if the user is already on the authentication page.
			if(response.config.redirect != false)
			{
				service = getStateRouterObject();
				if(service.current.name == '' || APP_CONFIG.nonCachedStates.indexOf(service.current.name) != -1)
				{
					showError = false;
				}
				service.go(APP_CONFIG.states.login);
				delayToProcess = 1000;
			}
			else
			{
				return true;
			}
		}

		//If the server returned a valid status code, handle it appropriately
		if(httpStatusCode >= 200 && httpStatusCode < 300)
		{
			//Anything inclusive in the range [200 - 299], is considered as a SUCCESS response
			return false;
		}

		//For any request status codes that lies outside [200 - 299], handle the error appropriately
		if(httpStatusCode == -1 && ((new Date()).getTime() - response.config.initial > response.config.timeout))
		{
			//This error happens only in case where a write request timed out.
			//In that case, the http status code is set as -1. We have to further check the 
			//time taken to differentiate it from connection not available error.
			httpStatusCode = "TIMEOUT";
			response.status = "TIMEOUT";
		}

		if(showError)
		{
			getIntervalService()(function(){
				handleHttpError(httpStatusCode);
			},delayToProcess,1);
		}
		return true;
	}

	function handleHttpError(status)
	{
		var messageObject = null;
        status = status.toString();

		//If the message Object was null, this means the server has thrown
		//one of the other http errors which we dont handle specifically.
		//Such cases will be treated as internal errors without showing the actual code.
		//We internally call this error pseudo internal error
		if(isInternalError(status))
		{
			messageObject = getInternalErrorMessageObject(null);
		}
		else
		{
			messageObject = HTTP_ERRORS[status];
			//If the backend threw an error that is not defined in the http errors constant,
			//treat this as a pseudo internall error. Show the same internal error message with
			//the http error status code
			if(messageObject == null)
			{
				messageObject = getInternalErrorMessageObject(status);
			}
		}

		
		//Send an error event to display the appropriate http error.		
		DialogueEventService.sendHttpErrorEventForDialog(messageObject);
	}

	function getInternalErrorMessageObject(status)
	{
		var messageObject = {
                title : null,
                message : null
            };

        var title = HTTP_ERRORS["INTERNAL_ERR"].title;
        var message = HTTP_ERRORS["INTERNAL_ERR"].message;
        
        if(status != null)
		{
			message += "("+status+")";
		}

        message += ". ";
        message += HTTP_ERRORS["ERROR_ASSISTANCE"].message;

        messageObject.title = title;
        messageObject.message = message;
        return messageObject;
	}

	function isInternalError(status)
	{
		return (status == "500" || status == "INTERNAL_ERR");
	}

	function glorifyResponse(response, message, isHttpError, isApiError)
	{
		if(response.data == null || response.data.constructor == String)
		{
			response.data = {};
		}

		var responseData = response.data;
		responseData.statusMessage = message;
		responseData.isHttpError = isHttpError;
		responseData.isApiError = isApiError;
		console.info("Glorified response object :: "+responseData);
	}

	function showUnknownStatusCodeError()
	{
		var messageObject = angular.copy(HTTP_ERRORS["UNKNOWN"]);
		DialogueEventService.sendHttpErrorEventForDialog(messageObject);
	}

	function isNotDefined(input)
	{
		if(input == null || input.length == 0 || (input.constructor != String && input.constructor != Number))
		{
			return true;
		}
		return false;
	}
}]);
