'use strict';
angular.module('twuizt')
.service('RefreshService', ['ActivityMonitor', 
	function(ActivityMonitor){
	var refreshService = {};

	refreshService.isRefreshAllowed = function()
	{
		// When the right sidenav is open
		// Check for left side nav is open
		// dialogues 
		// confirmation boxes 
		// <md-backdrop>
		// When the context menu
		if(angular.element("md-backdrop").length > 0)
		{
			console.warn("NO REFRESH - BACKDROP");
			return false;	
		}

		// toast
		//<md-toast>
		// if(angular.element("md-toast").length > 0)
		// {
		// 	console.warn("NO REFRESH - TOAST");
		// 	return false;
		// }

		if(angular.element(".md-scroll-mask").length > 0)
		{
			console.warn("NO REFRESH - DATE PICKER");
			return false;
		}

		if(ActivityMonitor.isUserActive() == false)
		{
			console.warn("NO REFRESH - USER INACTIVE");
			return false;
		}

		console.warn("REFRESH POSSIBLE");
		return true;
	};

 	return refreshService;
}]);