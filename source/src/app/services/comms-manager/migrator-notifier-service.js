'use strict';
angular.module('twuizt')
.service('TwuiztNotifier', ['$rootScope', '$localStorage', '$mdToast', '$interval', 
  'MIGRATOR_EVENTS',
  'MigratorCommService',
  function ($rootScope, $localStorage, $mdToast, $interval, 
    MIGRATOR_EVENTS,
    MigratorCommService)
{

  var twuiztNotifier = this;

  twuiztNotifier.notify = function(eventName, sendLocalBroadcast){
    console.log("Notifying event : "+eventName);
    var newEvent = {};
    newEvent.id = MigratorCommService.id;
    newEvent.event = eventName;

    console.info("Broadcasting "+eventName+" to all modules in the local app.");

    if(sendLocalBroadcast != false){
      $rootScope.$broadcast(eventName);
    }

    newEvent.timestamp = (new Date()).getTime();
    $localStorage.intercom = newEvent;
  }

  twuiztNotifier.inform = function(message){
    $mdToast.show(
                 $mdToast.simple()
                 .content(message)
                 .hideDelay(3000)
              );
  }

  twuiztNotifier.showInfo = function(message){
    $mdToast.show(
                 $mdToast.simple()
                 .content(message)
                 .hideDelay(3000)
                 .position("bottom left")
                 .parent(angular.element("body"))
              );
  }

  twuiztNotifier.showError = function(message){
    $mdToast.show(
                 $mdToast.simple()
                 .content(message)
                 .hideDelay(3000)
                 .theme('inlineToasterror')
                 .position("bottom left")
                 .parent(angular.element("body"))
              );
  }

  twuiztNotifier.sendInterTabNotification = function(){
    $interval(function(){
      twuiztNotifier.notify(MIGRATOR_EVENTS.INTERTAB_REFRESH);
    },0,1);
  }

}]);
