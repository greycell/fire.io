'use strict';
angular.module('twuizt')  
.service('MigratorCommService', ['$rootScope', '$window', 
  function ($rootScope, $window) 
{

  var timestamp = new Date().getTime();
  var storageState = {
    id : timestamp,
    lastUpdated : timestamp,
    eventsLocalCache : {}
  };

  if ($window.addEventListener){
    // For browsers other than IE
    $window.addEventListener("storage", handler, false);
  } 
  else {
    // for IE (why make your life more difficult)
    $window.attachEvent("onstorage", handler);
  };

  function handler(e) {

    console.info("Migrator App state synced : "+e.key);

    if(e.key != 'ngStorage-intercom'){
        return;
      }

    try{
          var localStorageStr = e.storageArea['ngStorage-intercom'];
          var intercom = JSON.parse(localStorageStr);

          console.info("Recieved an update on "+intercom);
          
          if(intercom != null && intercom.id != storageState.id && intercom.timestamp > storageState.lastUpdated){

              //Broadcast a notification event with the event that came
              storageState.lastUpdated = (new Date()).getTime();
              $rootScope.$broadcast(intercom.event);
              console.log("Event list : "+JSON.stringify(storageState.eventsLocalCache));
            }
      }
      catch(error){
        console.error(error);
      }
  }

  return storageState;

}]);