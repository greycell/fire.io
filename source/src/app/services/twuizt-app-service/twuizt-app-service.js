'use strict';
angular.module('twuizt')
    .service('TwuiztAppService', ['$q', '$rootScope', '$window', 
    	'APP_CONFIG', 'MIGRATOR_EVENTS',
    	'HttpService', 'TwuiztNotifier', 'MigratorHelper',
     function ($q, $rootScope, $window,
     	APP_CONFIG, MIGRATOR_EVENTS,
     	HttpService, TwuiztNotifier, MigratorHelper) {

    var twuiztAppService = this;
    var migratorVersion = "";
    var migratorServerTimeZone = null;
    var migratorServerCreateTimeStamp = null;

    var userName = null;

	var migratorClientServerUTCTimeDiffMs = 0;
    twuiztAppService.getServerTimeZone = function()
    {
    	return migratorServerTimeZone;
    };

    //service to get cached migrator version
	twuiztAppService.getVersionString = function() {	
		return migratorVersion;
	}

    //service to get cached system start time
	twuiztAppService.getSystemStartTimeUtc = function() {	
		return migratorServerCreateTimeStamp;
	}

    //service to get the cached servertime corrected to the server time
	twuiztAppService.getCurrentServerTime = function() {
		// var nowInMs = new Date().getTime();	
		// nowInMs = nowInMs - (nowInMs % 60000);
		// return  new Date(nowInMs + migratorClientServerUTCTimeDiffMs).getTime();
		return MigratorHelper.getLocalMillisForDateString() + migratorClientServerUTCTimeDiffMs;
	}

	//service to get cached migrator username
	twuiztAppService.getUserName = function() {	
		return userName;
	}
	//service to cache migrator user name
	twuiztAppService.setUserName = function(username,reload) {	
		if(reload){
			TwuiztNotifier.notify(MIGRATOR_EVENTS.MIGRATOR_RELOAD);	
		}
		userName=username;
	}

	//service to get migrator time from the backend
	twuiztAppService.getServerTime = function() {
	    var deferred = $q.defer();

	    sendRequestToFetchServerTime(deferred);
	    
	    return deferred.promise;
	}

	function sendRequestToFetchServerTime(deferred)
	{
		var URL=APP_CONFIG.baseWebApiUrl+'/serverTime';
	    var initial = new Date().getTime();
	    var final;

	    HttpService.sendHttp({
	            method: 'GET',
	            url: URL
	        })
	        .then(
	            function(response) {
	            	var final = new Date().getTime();
	            	var latency = final - initial;
	            	if(latency > 5000)
	            	{
	            		sendRequestToFetchServerTime(deferred);
	            		return;
	            	}

	            	migratorServerTimeZone = response.data.timeZone;
	            	migratorServerCreateTimeStamp = response.data.serverCreateTime;
	            	var currentServerTime = response.data.serverTime;
	            	var localTime = MigratorHelper.getLocalMillisForDateString();
            		console.warn("The server local time : "+MigratorHelper.getDateStringForLocalMillis(currentServerTime));
            		console.warn("The client local time : "+MigratorHelper.getDateStringForLocalMillis(localTime));

	            	var serverClientUTCTimeDiffMs = localTime - currentServerTime;
	            	var clientTimeCorrection = parseInt(serverClientUTCTimeDiffMs / 60000);
            		migratorClientServerUTCTimeDiffMs = clientTimeCorrection * 60000 * -1;
	            	
	            	if(clientTimeCorrection == 0)
	            	{
	            		console.warn("The client clock is in sync with the server UTC. Approx network latency : "+latency);
	            	}
	            	else if(clientTimeCorrection > 0)
	            	{
	            		console.warn("The client clock is faster than server by "+clientTimeCorrection+" minute(s). Approx network latency : "+latency);
	            	}
	            	else
	            	{
	            		console.warn("The client clock is slower than server by "+clientTimeCorrection+" minute(s). Approx network latency : "+latency);
	            	}
	            	console.log("SERVER TIME FETCH : ",response.data+' migratorClientServerUTCTimeDiffMs :: '+migratorClientServerUTCTimeDiffMs);
	                return deferred.resolve(response.data.serverTime);
	            },
	            function(response) {
	                console.error("Migrator server time fetch failed : " + JSON.stringify(response));
	                return deferred.reject(response);
	            }
	        );

	}


}
]);
