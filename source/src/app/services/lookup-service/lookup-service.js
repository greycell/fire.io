'use strict';
angular.module('twuizt')
.service('LookupService', ['$injector', 
	function($injector)
{
	var lookupService = this;
	var serviceCache = {};

	lookupService.getService = function(serviceName)
	{
		if(serviceCache[serviceName] == null)
		{
			try
			{
				serviceCache[serviceName] = $injector.get(serviceName);
			}
			catch(e)
			{
				return null;
			}
		}

		return serviceCache[serviceName];
	}
}]);
