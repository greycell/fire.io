'use strict';
angular.module('twuizt')  
.service('ArenaService', ['APP_CONFIG', 'HttpService', '$q', function(APP_CONFIG, HttpService, $q) 
{

  var arenaService = {};
  var URL=APP_CONFIG.baseWebApiUrl+'/player';
  
  // arenaService.getAllQuestions = function()
  // {
  //   var deferred = $q.defer();
  //   HttpService.sendHttp({
  //             method: 'GET',
  //             url: URL
  //         })
  //   .then(function(response){
  //     console.info("Successfully fetched all questions : "+JSON.stringify(response));

  //     deferred.resolve(response);
  //   },
  //     function(response){
  //     deferred.reject(response);
  //     });

  //   return deferred.promise;
  // };

  arenaService.activateOption = function(questionId, optionId)
  {
    var deferred = $q.defer();
    var payload = {
      action:"option",
      data:{
         questionId:questionId,
         optionId:optionId
      }
    };

    HttpService.sendHttp({
              method: 'POST',
              url: URL,
              timeout: 5000,
              data: payload
          })
    .then(function(response){
      console.info("Activate option response : "+JSON.stringify(response));

      deferred.resolve(response);
    },
      function(response){
      deferred.reject(response);
      });

    return deferred.promise;
  };


  return arenaService;

}]);