'use strict';
angular.module('twuizt')
.service('MigratorHelper', ['$rootScope', '$window', 
'MIGRATOR_EVENTS',
function($rootScope, $window, 
  MIGRATOR_EVENTS)
{
  var migratorHelper = this;
  var hourCycle = [12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  
  migratorHelper.getContextMenuItemsForListing = function() 
  {
    return [{ id: 1, name: "Open in New Window", action:"openNewWindow" },
        { id: 0, name: "Open", action:"openSameWindow" }
    ];
  };


  migratorHelper.forceUserToListing = function()
  {
    $rootScope.$broadcast(MIGRATOR_EVENTS.FORCE_USER_TO_LISTING);
  };

  migratorHelper.toLocalTime = function(millis)
  {
      //return millis+new Date(millis).getTimezoneOffset()*60000;
      return millis;
  };

  migratorHelper.getDateStringForLocalMillis = function(millis)
  {
    var dateObj;
    if(millis == null)
    {
      millis = getLocalMillisForDateString(new Date().toString());
    }

    dateObj = new Date(millis);

    return dateObj.toUTCString().slice(0,25);
  };

  migratorHelper.getLocalMillisForDateString = function(dateStr)
  {
    var dateObj;
    if(dateStr == null)
    {
      dateStr = new Date().toString();
    }

    dateStr = dateStr.slice(0,24);

    return new Date(dateStr+" UTC").getTime();
  };

  migratorHelper.openInNewWindow = function(urlToOpen) 
  {
      console.info("Opening url in new window : " + urlToOpen);
      return $window.open(urlToOpen, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=500, width=1000, height=600");
  };

  migratorHelper.isModelEmpty = function(obj)
  {
    if(obj == null || obj.toString().length == 0)
    {
      return true;
    }

    return false;
  }

  migratorHelper.toServerTime = function(millis)
  {
      // return millis-new Date(millis).getTimezoneOffset()*60000;
      return millis;
  };

  migratorHelper.formatTPutBBytes = function (Mbytes)
  {
    if(isNaN(Mbytes) || null==Mbytes || undefined==Mbytes || Mbytes<0) {
      return "";
    }

    var BYTECONV = 1024;
    var arrUnits = ['B','KB','MB','GB','TB','PB','EB', 'ZB', 'YB', 'BrB', 'GeB'];
    var bytes = Mbytes * Math.pow(BYTECONV,2);
    var nUnit = -1;
    var ctr=0;
    for(ctr=arrUnits.length; Math.floor(bytes/Math.pow(BYTECONV, ctr))<1 || ctr<0; ctr-- ){
      nUnit=ctr-1;
    }

    var strDisp= (bytes/Math.pow(BYTECONV, nUnit));
    strDisp = (isNaN(strDisp)?"": (Math.round(strDisp*100)/100));
    console.log("returning Disp String :"+(strDisp+" "+arrUnits[nUnit])+";;for Mbytes: "+Mbytes);
    return (strDisp+" "+arrUnits[nUnit]);
  };


  migratorHelper.constructAndFetchHoursList = function(time)
  {
    console.info("Constructing hour object for the time : "+time);
    var hours = [];
    var hour = null;
    for(var i = 0; i < 24; i++)                      
    {
      hour = {
        id : Math.random().toString(),
        hourValue : i,
        displayString : ""+hourCycle[i%12]+(i<12?" AM":" PM")
      };
      hours.push(hour);
    }

    if(time != null && !isNaN(time))
    {
      var date = new Date(time);
      var minutes = date.getMinutes();
      var hour = date.getHours();
      if(minutes > 0)
      {
        var displayString = ""+hourCycle[hour%12];
        displayString += ":";
        if(minutes < 10)
        {
          displayString += "0";
        }
        displayString += minutes;
        displayString += (hour<12?" AM":" PM");
        
        hour = {
          id : Math.random().toString(),
          hourValue : hour+(minutes/100),
          displayString : displayString
        };

        for(var i in hours)
        {
          if(hours[i].hourValue > hour.hourValue)
          {
            hours.splice(i, 0, hour);
            i=-1;
            break;
          }
        }
        if(i !== -1)
        {
          hours.push(hour);
        }
      }
    }

    return hours;
  }

  migratorHelper.fetchHourObjectFromHoursList = function(time, hoursList)
  {
    var date = new Date(time);
    var minutes = date.getMinutes();
    var hour = date.getHours();
    var displayString = ""+hourCycle[hour%12];
    if(minutes > 0)
    {
      displayString += ":";
      if(minutes < 10)
      {
        displayString += "0";
      }
      displayString += minutes;
    }

    displayString += (hour<12?" AM":" PM");

    for(var i in hoursList)
    {
      if(hoursList[i].displayString == displayString)
      {
        return hoursList[i];
      }
    }
    return null;
  }

  migratorHelper.fetchHourObjectFromHoursListById = function(hourObjId, hoursList)
  {
    for(var i in hoursList)
    {
      if(hoursList[i].id == hourObjId)
      {
        return hoursList[i];
      }
    }
    return null;
  }


  migratorHelper.getMilliSeconds = function(dateObj, hourObjId, hoursList)
  {

    date = new Date(dateObj.toString());
    var hourObj = migratorHelper.fetchHourObjectFromHoursListById(hourObjId, hoursList);

    date.setHours(hourObj.hourValue);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);

    return migratorHelper.toServerTime(date.getTime());
  }

  migratorHelper.getConsecutiveSyncsList = function()
  {
    return [{"id":"1", "optStr":"1"}, {"id":"2", "optStr":"2"}, {"id":"3", "optStr":"3"}, {"id":"4", "optStr":"4"}, {"id":"5", "optStr":"5"}, {"id":"6", "optStr":"6"}, {"id":"7", "optStr":"7"}, {"id":"8", "optStr":"8"}, {"id":"9", "optStr":"9"}];
  }

  migratorHelper.getDaysList=function(){
    return [{
            id: 0,
            day: "Sun"
        }, {
            id: 1,
            day: "Mon"
        }, {
            id: 2,
            day: "Tue"
        }, {
            id: 3,
            day: "Wed"
        }, {
            id: 4,
            day: "Thu"
        }, {
            id: 5,
            day: "Fri"
        }, {
            id: 6,
            day: "Sat"
        }];
  }

  migratorHelper.getOnlyModifiedFields=function(original, current){
      var modified=undefined;
      if(original==null){
        return current;
      }
      if(current==null){
        return current;
      }
      var k1 = Object.keys(original).sort();
      var k2 = Object.keys(current).sort();
      k1.forEach(function(key){
      var originalAttr=original[key];
      var currentAttr=current[key];
      if(typeof originalAttr == "object" &&  typeof currentAttr == "object"){

        
        var temp=migratorHelper.getOnlyModifiedFields(originalAttr,currentAttr);
        if(temp!=undefined){
          if(modified==undefined){
            modified={};
          }
          modified[key]=temp;
        }
      }
      else{
        if(originalAttr!=currentAttr){
            if(modified==undefined){
            modified={};
          }
          modified[key]=currentAttr;
        }
      }
    });

    return modified;
  };


  migratorHelper.getHoursList=function(){
    
   return [{
            id: 0,
            time: "12 AM"
        }, {
            id: 1,
            time: "1 AM"
        }, {
            id: 2,
            time: "2 AM"
        }, {
            id: 3,
            time: "3 AM"
        }, {
            id: 4,
            time: "4 AM"
        }, {
            id: 5,
            time: "5 AM"
        }, {
            id: 6,
            time: "6 AM"
        }, {
            id: 7,
            time: "7 AM"
        }, {
            id: 8,
            time: "8 AM"
        }, {
            id: 9,
            time: "9 AM"
        }, {
            id: 10,
            time: "10 AM"
        }, {
            id: 11,
            time: "11 AM"
        }, {
            id: 12,
            time: "12 PM"
        }, {
            id: 13,
            time: "1 PM"
        }, {
            id: 14,
            time: "2 PM"
        }, {
            id: 15,
            time: "3 PM"
        }, {
            id: 16,
            time: "4 PM"
        }, {
            id: 17,
            time: "5 PM"
        }, {
            id: 18,
            time: "6 PM"
        }, {
            id: 19,
            time: "7 PM"
        }, {
            id: 20,
            time: "8 PM"
        }, {
            id: 21,
            time: "9 PM"
        }, {
            id: 22,
            time: "10 PM"
        }, {
            id: 23,
            time: "11 PM"
        }];
  };

}]);
