'use strict';
angular.module('twuizt')
.service('ActiveEntityService', [function()
{
	var activeEntityService = {};
	var activeEntityString = "";
	var activeEntityId = null;

	activeEntityService.getActiveEntityIdString = function()
	{
		return activeEntityString;
	};

	activeEntityService.setActiveEntityIdString = function(entityString)
	{
		activeEntityString = entityString;
	};

	activeEntityService.clearActiveEntityIdString = function()
	{
		activeEntityString = "";
	};

	activeEntityService.getActiveEntityId = function()
	{
		return activeEntityId;
	};

	activeEntityService.setActiveEntityId = function(entityId)
	{
		activeEntityId = entityId;
	};

	activeEntityService.clearActiveEntityId = function()
	{
		activeEntityId = null;
	};

	return activeEntityService;
}]);
