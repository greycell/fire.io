'use strict';
angular.module('twuizt')
.service('MigratorLoader', ['$rootScope', 
  function($rootScope)
{
  var migratorLoader = {};

  migratorLoader.toggleGridLoader = function(isEnabled)
  {
    $rootScope.$broadcast('ToggleMigratorGridLoader', isEnabled);
  };

  migratorLoader.toggleStateChangeLoader = function(isEnabled)
  {
    $rootScope.$broadcast('ToggleMigratorUiLoader', isEnabled);
  };

  migratorLoader.toggleApiLoader = function(isEnabled)
  {
    $rootScope.$broadcast('ToggleMigratorUiLoader', isEnabled);
  };

  migratorLoader.ToggleMigratorWidgetLoader = function(action)
  {
    $rootScope.$broadcast('ToggleMigratorWidgetLoader', action);
  };

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams)
  {
    console.error("State change start - turning on ui loader");
    migratorLoader.toggleStateChangeLoader(true);
  });

  $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams)
  {
    migratorLoader.toggleStateChangeLoader(false);
  });

  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error)
  {
    console.error("State change error - turning off ui loader :: ",error);
    migratorLoader.toggleStateChangeLoader(false);
  });

  return migratorLoader;
}]);
