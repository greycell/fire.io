'use strict';
angular.module('twuizt')
.service('MigratorSuspender', ['$rootScope', '$state', 
	'APP_CONFIG', 
	function($rootScope, $state, 
		APP_CONFIG)
{
	var migratorSuspender = {};
	var isSuspenderActive = false;

	migratorSuspender.toggleSuspender = function(isEnabled)
	{
		isSuspenderActive = isEnabled;
		$rootScope.$broadcast('ToggleTwuiztInactivitySuspender', isEnabled);
	};

	migratorSuspender.isUiSuspended = function()
	{
		return isSuspenderActive;
	}

  return migratorSuspender;
}]);
