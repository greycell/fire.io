'use strict';
angular.module('twuizt')
    .service('HttpService', ['$http', 
        'MIGRATOR_CONSTANTS', 'HTTP_ERRORS', 
        'DialogueEventService', 
        function($http,  
            MIGRATOR_CONSTANTS, HTTP_ERRORS, 
            DialogueEventService) {

        //Create the headers object once to re-use it.
        var headerConstants = MIGRATOR_CONSTANTS.COMMON.HTTP_SERVICE_CONFIG;

        function glorifyHttpRequestConfig(config) {
            //Set a flag that differentiates the web api requests and the requests made my angular framework
            config.isWebApiRequest = true;

            //Set the appropriate headers if no headers are already set.
            if (config.headers == null) {
                config.headers = {};
            }
            config.headers[headerConstants.HEADER_KEY_ACCEPT] = headerConstants.HEADER_VALUE_ACCEPT;
            config.headers[headerConstants.HEADER_KEY_CONTENT_TYPE] = headerConstants.HEADER_VALUE_CONTENT_TYPE;

            //Check whether status codes and success codes are filled properly
    //         if(config.statusCodes == null || config.successCodes == null)
    //         {
    //         	console.error("Web api request does not have status codes and success status codes defined.");
				// config = null;
				// DialogueEventService.sendHttpErrorEventForDialog(angular.copy(HTTP_ERRORS["UNKNOWN"]));
    //         }

            return config;
        }

        var HttpService = this;

        HttpService.sendHttp = function(config) {
            return $http(glorifyHttpRequestConfig(config));
        };

        return HttpService;
    }]);
