'use strict';
angular.module('twuizt')  
.service('StatService', ['APP_CONFIG', 'HttpService', '$q', '$rootScope', function(APP_CONFIG, HttpService, $q, $rootScope) 
{

  var statService = this;
  
  var statData = {
    data:null,
    lastUpdated:null
  };

  var requestObject = {
    onAir:false,
    promise:null
  };

  statService.getAllStats = function()
  {
    var deferred = $q.defer();

      if(statData.lastUpdated == null)
      {
        if(requestObject.onAir == false)
        {
          requestObject.onAir = true;
          requestObject.promise = deferred.promise;

          var URL=APP_CONFIG.baseWebApiUrl+'/stats';
          HttpService.sendHttp({
                    method: 'GET',
                    url: URL
                })
          .then(function(response){
            console.info("Fetched all stats : "+JSON.stringify(response));
            statData.data = response.data;
            statData.lastUpdated = new Date().getTime();
            deferred.resolve(response.data);
            requestObject.onAir = false;
          },
            function(response){
                requestObject.onAir = false;
                deferred.reject(response);
            });        
        }
        else
        {
          return requestObject.promise;
        }
      }
      else
      {
        deferred.resolve(statData.data);
      }


      return deferred.promise;
  }
  
  $rootScope.$on('STAT_RELOAD_EVENT', function(event, data) {
            console.info("caught STAT_RELOAD_EVENT :: ", data);
            statData.data = data;
            statData.lastUpdated = new Date().getTime();
        });

  return statService;

}]);