'use strict';
angular.module('twuizt')
.service('MessageService', ['$location', 'HTTP_ERRORS', 'DialogueEventService', 
function($location,  HTTP_ERRORS, DialogueEventService)
{
	var messageService = this;

	messageService.getMessageString = function(statusCode, messages, params){
		
		var message = null;
		var regex = null; 
		try{
			if(isNotDefined(statusCode) || isNotObject(messages)){
				throw "ERROR";
			}

			message = messages[statusCode];

			if(isNotDefined(message)){
				throw "ERROR";
			}

			if(params != null && isNotObject(params) == false){
				var keys = Object.keys(params);
				var key;
				for(var index in keys){
					key = keys[index];
					regex = new RegExp("{{"+key+"}}", 'gim');
					message = message.replace(regex, params[key]);
				}
			}
		}
		catch(e){
			console.error("UNKNOWN CONSTANT KEY :: "+statusCode, messages);
			var messageObject = angular.copy(HTTP_ERRORS["UNKNOWN"]);
			DialogueEventService.sendHttpErrorEventForDialog(messageObject);
		}

		return message;
	}

	function isNotObject(object){
		if(object == null || object.constructor != Object){
			return true;
		}
		return false;
	}

	function isNotDefined(input){
		if(input == null || input.length == 0 || (input.constructor != Number && input.constructor != String))	{
			return true;
		}
		return false;
	}

}]);
