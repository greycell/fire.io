angular.module('twuizt')  
.service('SessionManager', ['$rootScope', 'APP_CONFIG', '$localStorage', function($rootScope, APP_CONFIG, $localStorage) 
{

  var sessionManager = this;
  var savedState = $localStorage['lastRequestedState'];
  console.error("CACHED STATE :: "+JSON.stringify(savedState));
  var states = APP_CONFIG.states;
  
  var nonCachedStates = APP_CONFIG.nonCachedStates;

  /*
  'redirect':'redirect',
        'baseAbstract' : 'admin-panel',
        'base' : 'admin-panel.authenticated',
        'defaultState':'admin-panel.authenticated.leaderboard',
        'login':'login',
  */

  if(savedState == null)
  {
    savedState = APP_CONFIG.states.defaultState;
  }
  else
  {
    savedState = savedState.name;

    //Check whether the saved state is a valid state anymore
    if(!isStateValid(savedState, states))
    {
      savedState = APP_CONFIG.states.defaultState;
    }
  }


  function isStateValid(stateName, partialStates)
  {
    var state;
    for(var i in partialStates)
    {
      state = partialStates[i];
      if(state != null && state.constructor == Object)
      {
        if(isStateValid(stateName, state))
        {
          return true;
        }
      }
      else if(nonCachedStates.indexOf(stateName) == -1 && stateName == state)
      {
        return true;
      }
    }
    return false;
  }

  var lastRequestedStateObj = {
    name : savedState,
    params : null
  };

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams)
  {
    if(nonCachedStates.indexOf(toState.name) == -1)
    {
      lastRequestedStateObj.name = toState.name;
      lastRequestedStateObj.params = toParams;
      $localStorage["lastRequestedState"] = lastRequestedStateObj;
      console.info("Saved the last request state object successfully");
    }
  });

  sessionManager.getToStateObject = function()
  {
    return lastRequestedStateObj;
  };

  return sessionManager;

}]);