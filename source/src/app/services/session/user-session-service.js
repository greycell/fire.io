angular.module('twuizt')  
.service('UserSession', ['$rootScope', 'APP_CONFIG', 'HttpService', '$q', 'SocketManager', function($rootScope, APP_CONFIG, HttpService, $q, SocketManager) 
{

  var userSession = {};
  
  var session = {
    user:null,
    isUserAuthenticated:false
  };


  userSession.isUserAuthenticated = function(redirectToLogin)
  {
    var deferred = $q.defer();
    var URL=APP_CONFIG.baseWebApiUrl+'/currentUser';
    HttpService.sendHttp({
              method: 'GET',
              url: URL,
              redirect:redirectToLogin==false?false:true
            })
    .then(function(response){
      console.info("The current logged in user : "+JSON.stringify(response));

      if(response.isApiError)
      {
        deferred.reject(response);
        return;
      }

      userSession.setUser(response.data);
      SocketManager.init(userSession);
      deferred.resolve(response.data);
    },
      function(response){
      deferred.reject(response);
      });

    return deferred.promise;
  }

  userSession.getUser = function()
  {
    return session.user;
  };


  userSession.getSession = function()
  {
    return session;
  };

  userSession.setUser = function(currentUser)
  {
    session.user = currentUser;
  };

  userSession.authenticate = function(tokenId)
  {
    var deferred = $q.defer();
    var URL=APP_CONFIG.baseWebApiUrl+'/authorizeUser';
    var payload = {
      tokenId : tokenId
    };
    HttpService.sendHttp({
              method: 'POST',
              url: URL,
              timeout: 30000,
              data: payload,
              redirect:false
          })
    .then(function(response){
      console.log("The user session response after authorization is : "+JSON.stringify(response));
      userSession.setUser(response.data);

      deferred.resolve(response);
    },
      function(response){
      deferred.reject(response);
      });

    return deferred.promise;
  }


  userSession.logout = function()
  {
    var deferred = $q.defer();
    var URL=APP_CONFIG.baseWebApiUrl+'/logout';
    HttpService.sendHttp({
              method: 'PUT',
              url: URL,
              timeout: 5000
          })
    .then(function(response){
      console.log("The user is successfully logged out : "+JSON.stringify(response));
      if(response.isApiError)
      {
        deferred.reject(response);
        return;
      }
      userSession.setUser(null);

      deferred.resolve(response);
    },
      function(response){
        console.error("The user was not logged out");
      deferred.reject(response);
      });

    return deferred.promise;
  }

  return userSession;

}]);