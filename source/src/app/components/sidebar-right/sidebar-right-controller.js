'use strict';

/**
 * @ngdoc function
 * @name SidebarRightController
 * @module migrator
 * @kind function
 *
 * @description
 *
 * Handles the right side bar view
 */
angular.module('twuizt').
controller('SidebarRightController', ['$scope', '$http', '$mdSidenav', '$state', '$document', 
  '$mdUtil', '$interval',
  function ($scope, $http, $mdSidenav, $state, $document, 
    $mdUtil,$interval) 
{
  
    // add a watch for when the url location changes
    $scope.$on('$stateChangeStart', function() {
        // location has changed so close menu
        $interval(function(){
          if($mdSidenav('rightSideBar').isOpen())
          {
            $mdSidenav('rightSideBar').close();
          }
        },0,1);
    });

    // add a watch for event of closing the right side nav, so 
    //the currently active element can be blurred to avoid button focussed state
    $scope.$on('CloseRightSideNav', function() {
        $document[0].activeElement.blur();
        // location has changed so close menu
        $interval(function(){
          if($mdSidenav('rightSideBar').isOpen())
          {
            $mdSidenav('rightSideBar').close();
          }
        },0,1);
    });

}]);
