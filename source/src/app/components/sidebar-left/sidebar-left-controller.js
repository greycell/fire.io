'use strict';

/**
 * @ngdoc function
 * @name SidebarLeftController
 * @module migrator
 * @kind function
 *
 * @description
 *
 * Handles the left sidebar
 */
angular.module('twuizt').
controller('SidebarLeftController', ['$scope', '$mdSidenav', '$location', '$http',
  '$rootScope', '$state', '$window', '$interval', '$filter', 
  'APP_CONFIG',
  'UserSession', 'TwuiztAppService', 'MigratorHelper',
  function($scope, $mdSidenav, $location, $http,
    $rootScope, $state, $window, $interval, $filter, 
    APP_CONFIG,
    UserSession, TwuiztAppService, MigratorHelper) {
    $scope.sidebarInfo = {
      appName: APP_CONFIG.name,
      appLogo: APP_CONFIG.logo
    };

    $scope.currentSession = UserSession.getSession();

    $scope.logout = function() {
      if ($mdSidenav('left').isOpen()) {
        $mdSidenav('left').close();
      }
      UserSession.logout()
      .then(
        function(response)
        {
          console.log("User is successfully logged out.");
          $state.go(APP_CONFIG.states.login);
        },
        function()
        {
          console.error("The user was not logged out.");
        })
    };

    // add a watch for when the url location changes
    $scope.$on('$locationChangeSuccess', function() {
      // location has changed so close menu
      $interval(function() {
        if ($mdSidenav('left').isOpen()) {
          $mdSidenav('left').close();
        }
      }, 0,1);
    });

    // add a watch for when the url location changes
    $scope.$on('$stateChangeStart', function() {
      // location has changed so close menu
      $interval(function() {
        if ($mdSidenav('left').isOpen()) {
          $mdSidenav('left').close();
        }
      }, 0,1);
    });

    // listener to close main menu during this event
    $scope.$on('CloseLeftSideMenu', function() {
      console.log("CloseLeftSideMenu event caught to close main menu.");
      if ($mdSidenav('left').isOpen()) {
        $mdSidenav('left').close();
      }
    });

    // $scope.openNewMigratorWindow = function() {
    //   var strIStr = $location.absUrl();
    //   var strPath = strIStr;
    //   var strTmp;
    //   if (strIStr.indexOf("/#/") > 3) {
    //     strPath = strIStr.substring((strIStr.indexOf("/#/") + 3));
    //     strPath = strPath.indexOf("/") > 0 ? strPath.substring(0, strPath.indexOf("/")) : strPath;
    //     strPath = strIStr.substring(0, strIStr.indexOf("/#/")) + "/#/" + strPath;

    //     console.log("Opening a new window for Module::::$state.$current::" + strPath);
    //   }
    //   $mdSidenav('left').close();
    //   $window.open(strPath, null, "width:1200");
    // };

  }
]);