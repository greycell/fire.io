'use strict';

/**
 * @ngdoc function
 * @name DefaultToolbarController
 * @module migrator
 * @kind function
 *
 * @description
 *
 * Handles all actions for toolbar
 */

angular.module('twuizt')
.controller('DefaultToolbarController', ['$mdDialog', '$scope', '$state', '$mdUtil', '$mdSidenav', 
    '$mdToast', '$rootScope', '$interval',
    'APP_CONFIG', 
    'SideMenu', 'UserSession', 'LeaderboardService', 'StatService',
    function ($mdDialog, $scope, $state, $mdUtil, $mdSidenav, 
    $mdToast, $rootScope,$interval, 
    APP_CONFIG, 
    SideMenu, UserSession, LeaderboardService, StatService) {
    $scope.menu = SideMenu.getMenu();
    $scope.toast = false;
    $scope.isToolbarMasked = false;
    $scope.currentSession = UserSession.getSession();
    $scope.toolbarObj = {
        color:"inherit"
    };
    $scope.getServerTime = LeaderboardService.getServerTime;

    $scope.openLeftSideBar = function() {
        $rootScope.$broadcast('CloseRightSideNav', null);
        $mdUtil.debounce(function(){
            $mdSidenav('left').toggle();
        }, 300)();
    };

    $scope.$on('ToggleMigratorUiLoader', function(broadcastEvent, maskToolBarEvent)
    {
        console.error("Is toolbar masked : "+maskToolBarEvent);
        $scope.isToolbarMasked = maskToolBarEvent;
    });
    
    $scope.$on('STAT_RELOAD_EVENT', function(event, data){
            console.info("caught STAT_RELOAD_EVENT in toolbar controller service:: ",data);
            processStats(data);
        });

    function processStats(data)
    {
        $scope.currentSession = UserSession.getSession();
        if($scope.currentSession.user.isAdmin == false)
        {
            if($scope.currentSession.user.team != null)
            {
                $scope.toolbarObj.color = data.teams[$scope.currentSession.user.team].color;
            }
            else
            {
                $scope.toolbarObj.color = "inherit";
            }    
        }    
    }

    $scope.closeRightSideBar = function(event)
    {
      if(!((event.target.localName=='md-icon')||(event.target.localName=='button'))) //check if the click is NOT on any of the icons.
      {
        /*IMPORTANT: remember to change if the icons were modified to material icon spec of <i> tag. currently we use only material design icon tags md-icon*/
        $interval(function(){
            $rootScope.$broadcast('CloseRightSideNav', null);
        },200,1);
      }
    };

    StatService.getAllStats()
    .then(
        function(response) {
            console.log("Stats response : " + JSON.stringify(response));
            processStats(response);
        },
        function(response){

        });

}]);
