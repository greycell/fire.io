//Global in-memory variable to store all incoming ips mapped to their test scenarios.

var STUB_SERVER_PORT = 8888;
var userImages_dir = "../src/assets/users/";

var OPEN_URL = 'authorizeUser';

var totalWebApisCounter = 0;
var SERVER_START_TIME = new Date();

var base_api_url = "/TwuiztApp/";
var admin_api_url = "admin/";

var defaultWebApiConfigObj = null;
var defaultTestScenario = null;
var currentRequestingServer = null;

var userMapperObject = {};
var adminMapperObject = {};
var userSocketMapper = {};
var sessionMapperObject = {};
var userStatsMapperObject = {};

var userDefinedTestScenariosObj = {};
var userDefinedTestScenariosList = [];
var testScenarioFileContentList = [];
var testScenarioIdList = [];
var invalidTestScenarioList = [];
var NOT_DEFINED = "<Not Available>";
var RESPONSE_IS_NULL = "Unable to find the response file configured in the test scenario.";
var serverCrashTime = 0;

var CONFIG = null;

var responseFileList = [];
var invalidResponseFileList = [];
var validResponseFileList = [];

var express = require('express');
var path = require('path');
var chalk = require('chalk');
var directoryReader = require('node-dir');
var bodyParser = require("body-parser");
var prompt = require('prompt');
var fs = require('fs');
var md5 = require('md5');
var lodash = require('lodash');

var QUESTION_STATES = {
	"NEW":"NEW",
	"PRESENTED":"PRESENTED",
	"ACTIVATED":"ACTIVATED",
	"DONE":"DONE",
	"DISCARDED":"DISCARDED"
};


var questionTimerObject = {
	timer:null
};

//This will enable the gzip compression
var compression = require('compression');
var app = express();
app.use(compression());
var cors = require('cors');
var session = require('express-session');
// var RedisStore = require('connect-redis')(session);
// app.use(express.cookieParser());
app.set('trust proxy', 1); // trust first proxy 
app.use(session({
	// genid: function(req) {
	// 	return Math.random();
 //    // return generateSessionId(req) // use UUIDs for session IDs 
 //  },
	// store: new RedisStore({
	//   host:'127.0.0.1',
	//   port:6379,
	//   prefix:'sess'
	// }),
  name:"TwuiztSession",
  resave: false,
  secret:"Twuizt",
  proxy:undefined,
  saveUninitialized: true,
  cookie: { secure: false,
  			path:"/",
  			// maxAge:null,
  			expires:60000*120,
  			httpOnly:true,
  			sameSite:true 
  		}
}));

//Initialize server dependencies
var $q = require('q');

function sanitizePattern(pattern){
	pattern = pattern.split(",");
	for(var i =0; i < pattern.length; i++){
		pattern[i] = parseInt(pattern[i]);
	}
	pattern = pattern.join(",");
	return pattern;
}

function getTemplateForConfigObject()
{

	//This is skeleton structure of the
	//expected config file. The types of the properties
	//must match. Otherwise the validation will fail
	return {
		"admins":["email"],
		"domains":["domain"],
		"questions":[{
			"id":0,
			"points":0,
			"status":"status",
			"negative":0,
			"question":"question",
			"timeout":0,
			"activatedAt":0,
			"options":[
				{
					"id":0,
					"label":"london",
					"isCorrect":true
				}]
		}]
	}
}

function initConfigFile()
{
	var configData = "./config.js";
	fs.readFile(configData, 'utf8', function (e, data) {

		if (e)
		{
			logger.error("Unable to initialize config data for Twuizt. "+e);
			process.exit();
			return;
		}

		try
		{
			CONFIG = JSON.parse(data);
			CONFIG.adminSettings = {
				"blacklistedPatterns":[{id:0, pattern:"1,2,3,4",patternLength:4}],
  				"buttonMatrix":{
				    "columns":8,
				    "rows":8
				}
			};
			
			for(i = 0; i<CONFIG.admins.length; i++)
			{
				CONFIG.admins[i] = CONFIG.admins[i].toLowerCase();
			}

			for(i = 0; i<CONFIG.domains.length; i++)
			{
				CONFIG.domains[i] = CONFIG.domains[i].toLowerCase();
			}
			
			logger.highlight("All defaut config settings have been loaded : "+JSON.stringify(CONFIG));
			initTwuizt();
		}
		catch(e)
		{
			logger.error("Unable to initialize config. "+e);
			process.exit();
			return;
		}
	});
}


function testForValidTwuiztConfig(defaultObjProperty, testObjProperty, parentProperty)
{
	// logger.info("Key to be analyzed : "+keyToBeAnalyzed);
	var keyToBeAnalyzed = parentProperty.name;
	var typeOfDefaultProperty = typeof(defaultObjProperty);
	if(testObjProperty == null)
	{
		// logger.error(JSON.stringify(defaultObjProperty)+" in test scenario is undefined. It should be of type "+((defaultObjProperty.constructor == Array)==true?"Array":"JSON Object"));
		var validation = {
			status : false,
			message : "'"+keyToBeAnalyzed+"' property in test scenario is undefined. It should be present and of type "+(typeOfDefaultProperty!="object"?typeOfDefaultProperty:((defaultObjProperty.constructor == Array)==true?"Array":"JSON Object"))
		};
		return validation;
	}

	var typeOfTestProperty = typeof(testObjProperty);

	// logger.highlight(keyToBeAnalyzed+" type in template : "+typeOfDefaultProperty);
	// logger.highlight(keyToBeAnalyzed+" type in test scenario : "+typeOfTestProperty);

	if(typeOfDefaultProperty == "object" && typeOfDefaultProperty == typeOfTestProperty)
	{
		//#1
		// logger.highlight("#1 Both properties are of type object.");
		var defaultObjectIsArray = defaultObjProperty.constructor == Array;
		var testObjectIsArray = testObjProperty.constructor == Array;
		if(defaultObjectIsArray != testObjectIsArray)
		{
			//#2
			// logger.warn("#2 Actual object type of both properties not equal.");
			// logger.error(defaultObjProperty+" in test scenario is "+(testObjectIsArray==true?"Array":"JSON Object")+". It should be of type "+(defaultObjectIsArray==true?"Array":"JSON Object"));
			var validation = {
				status : false,
				message : "'"+keyToBeAnalyzed+"' in test scenario is "+(testObjectIsArray==true?"Array":"JSON Object")+". It should be of type "+(defaultObjectIsArray==true?"Array":"JSON Object")
			};
			return validation;
		}

		if(defaultObjectIsArray)
		{
			//#3
			// logger.warn("#3 Boths are arrays.");
			//If its an array, parse through it using index.
			var validation;
			for(var index in testObjProperty)
			{
				// logger.highlight("Validating object at "+index);
				var _parentProperty = {
						name : parseInt(index)+1,
						type : "Array",
						parent : parentProperty.parent
					};
				validation  = testForValidTwuiztConfig(defaultObjProperty[0], testObjProperty[index], _parentProperty);
				if(validation.status === false)
				{
					return validation;
				}
			}
		}
		else
		{
			//#4
			// logger.warn("#4 Boths are json objects.");
			//If it is an object, parse through it using property.
			// for(var property in defaultObjProperty)
			// {
			// 	return testForValidTwuiztConfig(property, testObjProperty);
			// }
				var defaultKeys = Object.keys(defaultObjProperty).sort();
				var testObjKeys = Object.keys(testObjProperty).sort();
				// logger.warn("defaultKeys : "+defaultKeys);
				// logger.warn("testObjKeys : "+testObjKeys);

				//Check for invalid keys
				var invalidKeys = [];
				for(var i in testObjKeys)
				{
					if(defaultKeys.indexOf(testObjKeys[i]) == -1)
					{
						invalidKeys.push(testObjKeys[i]);
					}
				}

				if(invalidKeys.length > 0 && (parentProperty.type == "Array" || defaultKeys.length > 0))
				{
					var info;
					if(parentProperty.type == "Array")
					{
						info = " in the object no. '"+parentProperty.name+"' of the parent array '"+parentProperty.parent+"'"
					}
					else
					{
						info = " under its parent property '"+parentProperty.name+"'";

					}

					var validation = {
						status : false,
						message :"The test scenario object contains unknown keys : "+invalidKeys.join(", ")+info
					};
					return validation;
				}

				//Check for undefined keys
				var validation;
				for(var i in defaultKeys)
				{
					if(testObjKeys.indexOf(defaultKeys[i]) == -1)
					{
						//logger.error("'"+defaultKeys[i]+"' is not present in test scenario under parent property '"+keyToBeAnalyzed+"'");
						validation = {
						status : false,
						message :"'"+defaultKeys[i]+"' is not present in test scenario under parent property '"+parentProperty.name+"'"
					};
					return validation;
					}
					var _parentProperty = {
						name : defaultKeys[i],
						type : "Object",
						parent : defaultKeys[i]
					};
					validation = testForValidTwuiztConfig(defaultObjProperty[defaultKeys[i]], testObjProperty[defaultKeys[i]], _parentProperty);
					if(validation.status === false)
					{
						return validation;
					}
				}
		}

	}
	else if(typeOfDefaultProperty == "object" && typeOfTestProperty != "object")
	{
		//#5
		// logger.error("#5 '"+keyToBeAnalyzed+"' type in test scenario is '"+typeOfTestProperty+"'. It should be of type '"+(defaultObjectIsArray==true?"Array":"JSON Object")+"'");
		var validation = {
				status : false,
				message : "'"+keyToBeAnalyzed+"' type in test scenario is '"+typeOfTestProperty+"'. It should be of type '"+(defaultObjectIsArray==true?"Array":"JSON Object")+"'"
			};
		return validation;
	}
	else
	{
		//#6
		// logger.warn("'"+keyToBeAnalyzed+"' detected as leaf node in template of type :: "+typeOfDefaultProperty);
		// logger.warn("'"+keyToBeAnalyzed+"' detected as leaf node in test scenario of type :: "+typeOfTestProperty);
		//this is leaf property and no longer an object
		//compare and return
		if(typeOfDefaultProperty != typeOfTestProperty)
		{
			//#7
			var validation = {
				status : false,
				message : "'"+keyToBeAnalyzed+"' in test scenario is of type '"+typeOfTestProperty+"'. It should be of type '"+typeOfDefaultProperty+"'."
			};
			return validation;
		}

	}

	// logger.highlight("Passed true in interation ");
	var validation = {
				status : true,
				message : ""
	};
	return validation;
}


initConfigFile();

var server = require('http').Server(app);
var io = null;
io = require('socket.io')(server, {path: '/twuiztSocket'});
		io.on('error', function (socket) {
			logger.error("COnnection error.");
		});
		
		io.on('connection', function (socket) {

			logger.highlight("SOCKET IO CONNECTION INIT : "+JSON.stringify(socket.id)+" :: "+JSON.stringify(socket.handshake.query.userId));
			var userId = socket.handshake.query.userId;
			userSocketMapper[userId] = socket;
			// socket.emit('message', {'message': 'hello world'});

			// sendSocketEventToAllClients("CONNECTION_OK", "test");

			socket.on('message', function (from, msg) {

			  console.log('recieved message from', 
			              from, 'msg', JSON.stringify(msg));

			  console.log('broadcasting message');
			  console.log('payload is', msg);
			  // io.sockets.emit('broadcast', {
			  //   payload: msg,
			  //   source: from
			  // });
			
			// sendSocketEventToAllClients("twuiztClient", "refresh", msg);

			  console.log('broadcast complete');
			});

			socket.on('disconnect',function(){
				console.log('client has disconnected');
			});

			socket.on('connect',function(){
				console.log('client has connected');
				
				// sendSocketEventToAllClients("CONNECTION_OK", "test");
			});

		});
// io.serveClient(true);
// app.use(cors);
app.options('*', cors());

var whitelist = ['http://localhost:3000'];
var corsOptions = {
  origin: function(origin, callback){
    var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
    callback(originIsWhitelisted ? null : 'Bad Request', originIsWhitelisted);
  }
};

var requestCounter = 0;
var logger = {};


//Configure the express app to use body-parser to read post parameters.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// io.on('connection', function(socket) {
//   console.log('new connection');

//   socket.on('add-customer', function(customer) {
//     io.emit('notification', {
//       message: 'new customer',
//       customer: customer
//     });
//   });
// });

function alertAllOnlineAdmins(event){
	var admin;
	var stats = getStatsObject(true);
	for(i in adminMapperObject){
		if(userSocketMapper[i]){
			sendSocketMessage(userSocketMapper[i].id, 'twuiztAdmin', event, stats);
		}
	}
}

function sendSocketEventToAllClients(channelName, eventName, data)
{
	logger.warn("Sending socket");
	io.sockets.emit(channelName, eventName, data);
}

function sendSocketMessage(socketid, channelName, eventName, data){
	io.sockets.to(socketid).emit(channelName, eventName, data);
}

logger.log = function(message)
{
	console.log(chalk.blue("LOG : "+message));
}

logger.error = function(message)
{
	console.log(chalk.red("ERROR : "+message));
}

logger.debug = function(message)
{
	console.log(chalk.grey("DEBUG : "+message));
}

logger.info = function(message)
{
	console.log(chalk.cyan("INFO : "+message));
}

logger.warn = function(message)
{
	console.log(chalk.magenta("WARN : "+message));
}

logger.highlight = function(message)
{
	console.log(chalk.yellow("INFO : "+message));
}


function syntaxHighlight(json)
{
	if(json == null)
	{
		return "";
	}

    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';

        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });

    return "<pre>"+json+"</pre>";
}


function compareObjectsForEquality( x, y )
{
    if ( x === y )
    {
        return true;
        // if both x and y are null or undefined and exactly the same
    }

    if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) )
    {
        return false;
    // if they are not strictly equal, they both need to be Objects
    }

    if ( x.constructor !== y.constructor )
    {
        return false;
        // they must have the exact same prototype chain, the closest we can do is
        // test there constructor.
    }

  for ( var p in x )
  {
    if ( ! x.hasOwnProperty( p ) ) continue;
      // other properties were tested using x.constructor === y.constructor

    if ( ! y.hasOwnProperty( p ) ) return false;
      // allows to compare x[ p ] and y[ p ] when set to undefined

    if ( x[ p ] === y[ p ] ) continue;
      // if they have the same strict value or identity then they are equal

    if ( typeof( x[ p ] ) !== "object" ) return false;
      // Numbers, Strings, Functions, Booleans must be strictly equal

    if ( ! compareObjectsForEquality( x[ p ],  y[ p ] ) ) return false;
      // Objects and Arrays must be tested recursively
  }

  for ( p in y )
  {
    if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) return false;
      // allows x[ p ] to be set to undefined
  }
  return true;
}

function sendHttpError(res, code, message)
{
	sendResponse(res, code, "FAILURE", message, null);
}

function sendApiError(res, code, message)
{
	var codeToSend = code==null?"FAILURE":code;
	var message = message;
	sendResponse(res, 200, codeToSend, message, null);
}

function sendResponse(res, code, apiStatus, apiStatusMessage, data)
{
	res.status(code).send({ 
		apiStatus:apiStatus,
		apiStatusMessage: apiStatusMessage,
		data:data
	});
}

function sendSuccessResponse(res, data)
{
	sendResponse(res, 200, "SUCCESS", "SUCCESS", data);
}

var downloadUserImage = function(uri, filename, callback){
	var fs = require('fs');
	var request = require('request');
  	request.head(uri, function(err, res, body){
    // console.log('content-type:', res.headers['content-type']);
    // console.log('content-length:', res.headers['content-length']);
    if(err)
    {
    	logger.error("User download image :: "+err);
    	return;
    }
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

//This function is also called during request pre-processing to set the cache control in the headers.
function setResponseHeaders(res)
{
	res.header("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
	// res.header("Cache-Control", "npost-check=0, pre-check=0");
	res.header("Pragma", "no-cache");
	return res;
}

//This function fetches the trimmed ip address from the request headers
function fetchSanitizedIpFromReqObj(req)
{
	var sanitizedIp = req.headers["originalip"];
	if(sanitizedIp == null)
	{
		sanitizedIp = req.connection.remoteAddress;
	}
	sanitizedIp = sanitizedIp.split(":");
	sanitizedIp = sanitizedIp[sanitizedIp.length-1];
	return sanitizedIp.split(".").join("_");
}

//This function fetches the trimmed ip address from the request headers
function fetchUserInCurrentSession(sessionId)
{
	logger.highlight("User store :: "+JSON.stringify(userMapperObject));
	logger.highlight("Session store :: "+JSON.stringify(sessionMapperObject));
	var userId = sessionMapperObject[sessionId];
	if(userId == null)
	{
		return null;
	}

	return userMapperObject[userId];
}


function getResponseObject(statusCode, dataObj)
{
	var response = {
		apiStatus : statusCode
	};

	if(dataObj != null)
	{
		response.data = dataObj;
	}
	else
	{
		response.data = null;
	}

	return JSON.stringify(response);
}

function getStatsObject(isAdmin)
{
	try
	{
		var stats = {
			buttonMatrix:CONFIG.adminSettings.buttonMatrix,
			blacklistedPatterns:CONFIG.adminSettings.blacklistedPatterns,
			serverTime:new Date().getTime()
		};

		if(isAdmin){
			stats.userStats = userStatsMapperObject
			stats.userInfo = userMapperObject
		}
	
		return stats;
	}
	catch(err)
	{
		logger.error("getStatsObject() :: "+ err);
	}
}


function getStatForUser(req){
	var currentUser = fetchUserInCurrentSession(req.session.id);
	var data = getStatsObject(currentUser.isAdmin);
	data.currentUser = currentUser;
	data.currentUserStats = userStatsMapperObject[data.currentUser.userId];
	return data;
}

function initTwuizt()
{
	//Preprocessing of all incoming requests. Used to map ip addresses to test scenarios
	//This function will always get called no matter what the request is.
	app.use(function(req, res, next) {
		requestCounter++;
		var ip = req.headers["originalip"];
		if(ip == null)
		{
			ip = req.connection.remoteAddress;
		}
		res.io = io;
	  	logger.highlight("Incoming request from : "+ip+" with session id : "+req.session.id+" SESSION ID :: "+req.sessionID);
	  	logger.highlight("Requests served : "+requestCounter);
	  	logger.highlight("Request url : "+req.url+"::"+req.url.toLowerCase()+"::"+(base_api_url+OPEN_URL).toLowerCase());

	  	var userId = sessionMapperObject[req.session.id];

	  	if( userId == null && req.url.toLowerCase() != (base_api_url+OPEN_URL).toLowerCase())
	  	{
			sendHttpError(res, 401, "The user does not have a valid session.");
			return;
	  	}

	  	if(req.url.toLowerCase().indexOf((base_api_url+admin_api_url).toLowerCase()) == 0 && userMapperObject[userId].isAdmin == false)
	  	{
	  		sendHttpError(res, 403, "The user does not sufficient permissions to do this operation.");
			return;	
	  	}

	  	currentRequestingServer = req.protocol + '://' + req.get('host');
	  	//Set the appropriate response headers here
	  	setResponseHeaders(res);

		next();
	});

	/*
	*
	*
	*	The apis for Twuizt server are configured below
	*
	*
	*/

	// Access the session as req.session 
	app.get(base_api_url+'getSession', function(req, res, next) {
	  // var sess = req.session
	  logger.highlight("Current session : "+JSON.stringify(req.session));
	  try{
		  var data = {};
			data.session = req.session;
		    data.sessionid = req.session.id;
		    data.sessionID = req.sessionID;
		  if (data.session.views) {
		    data.session.views++
		  } else {
		    data.session.views = 1
		  }

		  	data.sessionStore = JSON.stringify(sessionMapperObject);
		  	data.userStore = JSON.stringify(userMapperObject);

			sendSuccessResponse(res, data);
	  }
	  catch(err)
	  {
		logger.error(req.url +" :: "+ err);
	  }
	})


		//API to fetch the current logged user
		app.get(base_api_url+'currentUser', function(req,res){
			try{
				logger.highlight("Fetching current User.");
				logger.info("Current session id : "+req.session.id+"::"+req.sessionID+"::"+JSON.stringify(req.session));
				var user = fetchUserInCurrentSession(req.session.id);
				logger.info("Current user in session : "+req.session.id+"::"+JSON.stringify(user));
				sendSuccessResponse(res, user);
			}
			catch(err)
			{
				logger.error(req.url +" :: "+ err);
			}
		});

		//API to fetch the server time
		app.get(base_api_url+'serverTime', function(req,res){
			try
			{
				var data = {};
				data.serverTime = new Date().getTime();
				sendSuccessResponse(res, data);
				// sendSocketEventToAllClients("twuiztClient", "reload", data);
				return;
			}
			catch(err)
			{
				logger.error(req.url +" :: "+ err);
				return;
			}
		});

		//API to log the user out
		app.put(base_api_url+'logout', function(req,res){
			try
			{
				var data = {};
				req.session.destroy(function(err) {
				  // cannot access session here
				  if(err)
				  {
				  	logger.error("The user session was not destroyed : "+err);
				  	return;
				  }
				  logger.highlight("The user session was successfully destroyed.");
				})
				delete sessionMapperObject[req.sessionID];
				sendSuccessResponse(res, data);
				// sendSocketEventToAllClients("twuiztClient", "reload", data);
				alertAllOnlineAdmins('userStats');
				return;
			}
			catch(err)
			{
				logger.error(req.url +" :: "+ err);
				return;
			}
		});



		//API to fetch realtime stats
		app.get(base_api_url+'stats', function(req,res){
			try
			{
				sendSuccessResponse(res, getStatForUser(req));
				return;
			}
			catch(err)
			{
				logger.error(req.url +" :: "+ err);
				return;
			}
		});

		//API to manage player actions
		app.post(base_api_url+'threat', function(req, res) {
			logger.highlight("API called to register a threat"+JSON.stringify(req.body));
			try
			{
				var data = req.body;
				var userSession = fetchUserInCurrentSession(req.session.id);
				var userId, i;

				if(userSession == null)
				{
					sendApiError(res, "INVALID_USER", "The requested user does not exist.")
					return;	
				}
				userId = userSession.userId;
				var currentTime = new Date().getTime();
				for(i=0; i<data.length; i++){
					data[i].ts = currentTime;
				}
				var userObject = userStatsMapperObject[userSession.userId];
				userObject.threats = userObject.threats.concat(data);
				
				alertAllOnlineAdmins('threat_alert');

				sendSuccessResponse(res, getStatForUser(req));

			}
			catch(err)
			{
				logger.error(req.url +" :: "+ err);
				return;
			}
		});


		//API to manage settings
		app.post(base_api_url+admin_api_url+'settings', function(req, res) {
			logger.highlight("API called to manage settings : "+JSON.stringify(req.body));
			var sanitizedIp = fetchSanitizedIpFromReqObj(req);
			try
			{
				var action = req.body.action;
				var data = req.body.data;
				if(action == null)
				{
					sendApiError(res, "INVALID_PARAMS", "The request contained invalid params.")
					return;
				}


				switch(action.toString().toLowerCase())
				{
					case 'button_settings':
						logger.highlight("request is to update the button settings");
						var columns = parseInt(data.columns);
						var rows = parseInt(data.rows);
						if(isNaN(columns) || isNaN(rows)){
							sendApiError(res, "INVALID_PARAMS", "The request contained invalid params.")
							return;			
						}
						
						CONFIG.adminSettings.buttonMatrix.columns = columns;
						CONFIG.adminSettings.buttonMatrix.rows = rows;

					break;

					case 'add':
					var regEx = new RegExp(/^(0*[1-9]\d*)(\s*\,\s*(0*[1-9]\d*))+\s*$/);
					if(data.pattern == null || !regEx.test(data.pattern))
					{
						sendApiError(res, "INVALID_PARAMS", "The request contained invalid params.")
						return;
					}
					logger.highlight("Request is to add a new pattern.");
					var pattern = sanitizePattern(data.pattern);
					var patternLength = pattern.split(",").length;
					var index = lodash.findIndex(CONFIG.adminSettings.blacklistedPatterns, {pattern:pattern});
					if(index != -1){
						sendApiError(res, "DUPLICATE", "Pattern already exists.")
						return;	
					}
					CONFIG.adminSettings.blacklistedPatterns.push({
						id:Math.random().toString().split(".")[1],
						pattern:pattern,
						patternLength:patternLength
					});

					break;

					case 'edit':
					var regEx = new RegExp(/^(0*[1-9]\d*)(\s*\,\s*(0*[1-9]\d*))+\s*$/);
					if(data.id == null || data.pattern == null || !regEx.test(data.pattern))
					{
						sendApiError(res, "INVALID_PARAMS", "The request contained invalid params.")
						return;
					}
					logger.highlight("Request is to edit an existing pattern.");
					var index = lodash.findIndex(CONFIG.adminSettings.blacklistedPatterns, {id:data.id});
					if(index == -1)
					{
						sendApiError(res, "INVALID_ID", "The requested pattern does not exist.")
						return;
					}

					var pattern = sanitizePattern(data.pattern);
					var patternLength = pattern.split(",").length;
					var patternIndex = lodash.findIndex(CONFIG.adminSettings.blacklistedPatterns, {pattern:pattern});
					console.log("Index : ",index);
					if(patternIndex != -1){
						sendApiError(res, "DUPLICATE", "Pattern already exists.")
						return;	
					}
					console.log("Pattern : ",pattern);
					CONFIG.adminSettings.blacklistedPatterns[index].pattern = pattern;
					CONFIG.adminSettings.blacklistedPatterns[index].patternLength = patternLength;

					break;

					case 'delete':
					if(data.id == null)
					{
						sendApiError(res, "INVALID_ID", "The requested pattern does not exist.")
						return;
					}
					logger.highlight("Request is to delete an existing pattern.");
					var index = lodash.findIndex(CONFIG.adminSettings.blacklistedPatterns, {id:data.id});
					if(index == -1)
					{
						sendApiError(res, "INVALID_ID", "The requested pattern does not exist.")
						return;
					}
					delete CONFIG.adminSettings.blacklistedPatterns.splice(index, 1);

					break;

				}
				var stats = getStatsObject();
    			sendSocketEventToAllClients("twuiztClient", "refresh", stats);

    			stats = getStatsObject(true);
    			alertAllOnlineAdmins('userStats');

				sendSuccessResponse(res, stats);			
			}
			catch(err)
			{
				logger.error(req.url +" :: "+ err);
				sendHttpError(res, 500, err);			
			}
		});


		//API to set email id mapped to an ip authorizeUser
		app.post(base_api_url+OPEN_URL, function(req, res) {
			logger.highlight("Fetching current User.");
			var sanitizedIp = fetchSanitizedIpFromReqObj(req);
			try
			{
				var access_token = req.body.tokenId;
				var sanitizedIp = fetchSanitizedIpFromReqObj(req);
				logger.warn("Authenticating user with tokenid : "+access_token);
				var request = require('request');

				request('https://www.googleapis.com/plus/v1/people/me?access_token=' + access_token, function(err, response, body) {
				  if (err) 
				  {
					logger.error(req.url +" :: "+ err);
					sendHttpError(res, 401, err);
					return;
				  }

				  if (res.statusCode != 200) {
					logger.error("Invalid access token. Failed to retrieve user info. Status code : ",res.statusCode);
					sendHttpError(res, 401, "Invalid access token. Failed to retrieve user info.");
					return;
				  }
				  else {
				    var me;
				    try { 

				    		// me = JSON.parse(response);
							// var emailId = fetchEmailIdFromSanitizedIp(sanitizedIp);
							var result = JSON.parse(response.body);
				    		logger.info('user profile:'+JSON.stringify(result));
				    		var email = result.emails[0].value;
				    		logger.info('user email:'+JSON.stringify(email));
				    		var userDomain = email == null?"":email.split("@")[1];
				    		if( CONFIG.admins.indexOf(email==null?"":email.toLowerCase()) == -1 && CONFIG.domains.indexOf(userDomain==null?"":userDomain.toLowerCase()) == -1)
				    		{
				    			sendApiError(res, "NOT_AUTHORIZED", "You need to have a vizury account to be able to play this game.");
				    			return;
				    		}
				    		
				    		logger.highlight("User object from google :: "+JSON.stringify(result));

				    		var userId = result.id;
				    		var userName = result.displayName;


				    		logger.info("sanitizedIp:"+sanitizedIp+' user id:'+userId+" email: "+email+" name: "+userName+" hash:"+userMapperObject[userId]);

							downloadUserImage(result.image.url, userImages_dir+userId+'.jpg', function(err){
								if(err)
								{
									logger.error(req.url +" :: "+ err);
								}

								var isAdmin = false;
								var roles  = [];
								if(CONFIG.admins.indexOf(email.toLowerCase()) != -1)
								{
									isAdmin = true;
									roles = ["0"];
								}
								else
								{
									roles = ["1"];
								}	

								var previousUserObject = userMapperObject[userId];
					    		var dataToSend = {
					    			image:"/assets/users/"+userId+".jpg",
					    			userId:userId,
					    			userName:userName,
					    			email:email,
					    			isAdmin:isAdmin,
					    			roles:roles
					    		};

					    		if(isAdmin){
									adminMapperObject[userId] = dataToSend;
					    		}

				    			userMapperObject[userId] = dataToSend;
				    			sessionMapperObject[req.session.id] = userId;
				    			if(userStatsMapperObject[userId] == null)
								{
									userStatsMapperObject[userId] = {
										threats:[]
									};	
								}
								alertAllOnlineAdmins('userStats');

								sendSuccessResponse(res, dataToSend);
							});

				    	}
				    catch (e) {
							logger.error(e);
							sendHttpError(res, 500, e);
				    }

				  }
				});
			}
			catch(e)
			{
				logger.error(e);
				sendHttpError(res, 500, e);
			}

		});

		try
		{
			logger.info("Attempting to start stub server.");
			server.listen(STUB_SERVER_PORT);

			logger.highlight("Stub server is listening at port : "+STUB_SERVER_PORT);
		}
		catch(e)
		{
			logger.error("Failed to start the stub server. Reason : "+e);
		}

}