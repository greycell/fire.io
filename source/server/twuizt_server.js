//Global in-memory variable to store all incoming ips mapped to their test scenarios.

var STUB_SERVER_PORT = 8000;
var data_dir = './data/';
var default_dir = './data/test-scenarios/default/';
var testScenario_dir = './data/test-scenarios/';
var totalWebApisCounter = 0;
var SERVER_START_TIME = new Date();

var base_api_url = "/migratorApp/";
var defaultWebApiConfigObj = null;
var defaultTestScenario = null;
var currentRequestingServer = null;

var ipMapperObject = {};
var userDefinedTestScenariosObj = {};
var userDefinedTestScenariosList = [];
var testScenarioFileContentList = [];
var testScenarioIdList = [];
var invalidTestScenarioList = [];
var NOT_DEFINED = "<Not Available>";
var RESPONSE_IS_NULL = "Unable to find the response file configured in the test scenario.";
var serverCrashTime = 0;

var responseFileList = [];
var invalidResponseFileList = [];
var validResponseFileList = [];

var express = require('express');
var path = require('path');
var chalk = require('chalk');
var directoryReader = require('node-dir');
var bodyParser = require("body-parser");
var prompt = require('prompt');
var fs = require('fs');
//This will enable the gzip compression
var compression = require('compression');
var app = express();
app.use(compression());


var requestCounter = 0;
var logger = {};


//Configure the express app to use body-parser to read post parameters.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

logger.log = function(message)
{
	console.log(chalk.blue("LOG : "+message));
}

logger.error = function(message)
{
	console.log(chalk.red("ERROR : "+message));
}

logger.debug = function(message)
{
	console.log(chalk.grey("DEBUG : "+message));
}

logger.info = function(message)
{
	console.log(chalk.cyan("INFO : "+message));
}

logger.warn = function(message)
{
	console.log(chalk.magenta("WARN : "+message));
}

logger.highlight = function(message)
{
	console.log(chalk.yellow("INFO : "+message));
}


function syntaxHighlight(json)
{
	if(json == null)
	{
		return "";
	}

    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';

        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });

    return "<pre>"+json+"</pre>";
}


function compareObjectsForEquality( x, y )
{
    if ( x === y )
    {
        return true;
        // if both x and y are null or undefined and exactly the same
    }

    if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) )
    {
        return false;
    // if they are not strictly equal, they both need to be Objects
    }

    if ( x.constructor !== y.constructor )
    {
        return false;
        // they must have the exact same prototype chain, the closest we can do is
        // test there constructor.
    }

  for ( var p in x )
  {
    if ( ! x.hasOwnProperty( p ) ) continue;
      // other properties were tested using x.constructor === y.constructor

    if ( ! y.hasOwnProperty( p ) ) return false;
      // allows to compare x[ p ] and y[ p ] when set to undefined

    if ( x[ p ] === y[ p ] ) continue;
      // if they have the same strict value or identity then they are equal

    if ( typeof( x[ p ] ) !== "object" ) return false;
      // Numbers, Strings, Functions, Booleans must be strictly equal

    if ( ! compareObjectsForEquality( x[ p ],  y[ p ] ) ) return false;
      // Objects and Arrays must be tested recursively
  }

  for ( p in y )
  {
    if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) return false;
      // allows x[ p ] to be set to undefined
  }
  return true;
}



//Preprocessing of all incoming requests. Used to map ip addresses to test scenarios
//This function will always get called no matter what the request is.
app.use(function(req, res, next) {
	requestCounter++;
	var ip = req.headers["originalip"];
	if(ip == null)
	{
		ip = req.connection.remoteAddress;
	}
  	logger.highlight("Incoming request from : "+ip);
  	logger.highlight("Requests served : "+requestCounter);
  	currentRequestingServer = req.protocol + '://' + req.get('host');
  	//Set the appropriate response headers here
  	setResponseHeaders(res);

  	//Check whether the ip address is already mapped in the ipMapperObject
	processRequestAndLoadTestScenario(req, res);

	next();
});

initializeStubServer();

function initializeStubServer()
{
	logger.info("Initializing stub server. This may take a while depending on the number of test scenario files to load.");
	loadAllInitialDataSources();
}


function loadAllTestScenarios()
{
	logger.info("Loading all test scenarios to memory.");
	// match only filenames with a .json extension
	directoryReader.readFiles(testScenario_dir, {
	    match: /^TS_.*\.json$/i,
	    // exclude: /^\./
	    }, function(err, content, next) {
	        if (err){
	         throw err;
			}

			testScenarioFileContentList.push(content);

	        next();
	    },
	    function(err, files){
	        if (err) {
	        	throw err;
	        }

	        logger.highlight('Successfully loaded all available test scenario files to memory.'+files);
	        logger.info("Preparing to validate all loaded test scenarios.");

	        // logger.error(JSON.stringify(userDefinedTestScenariosObj));

	        if(files.length > 0)
	        {
	        	validateTestScenariosInMemory(files);
	        }
	        else
	        {
	        	registerWebApis();
	        }

	        // logger.error(JSON.stringify(userDefinedTestScenariosList));

	    });
}


   /*
	*
	*
	*	The apis for Twuizt server are configured below
	*
	*
	*/




	//This url returns all the test defined test scenarios to display in test framework
	app.get('/testapi/getAllTestScenarios', function(req, res) {
		var sanitizedIp = fetchSanitizedIpFromReqObj(req);
		logger.highlight(sanitizedIp+" : Fetched all test scenarios.");

		res.writeHead(200, {
        'Content-Type': 'application/json'
    	});
    	var testScenarios = {};
    	testScenarios.activeScenarioId = getIpMapperObject()[sanitizedIp].activeTestScenario.id;
    	testScenarios.testScenarios = userDefinedTestScenariosList;
		res.end(JSON.stringify(testScenarios));
	});


	//This url returns the currently active scenario with its status and details
	app.get('/testapi/getActiveScenario', function(req, res) {
		var sanitizedIp = fetchSanitizedIpFromReqObj(req);
    	var activeScenario = getIpMapperObject()[sanitizedIp];

		logger.highlight(sanitizedIp+" : Fetched active scenario with id : "+activeScenario.activeTestScenario.id);

		res.writeHead(200, {
        'Content-Type': 'application/json'
    	});
    	activeScenario.invalidTestScenarios = invalidTestScenarioList;
    	activeScenario.requestCounter = requestCounter;
    	activeScenario.serverStartTime = SERVER_START_TIME;
		res.end(JSON.stringify(activeScenario));
	});

	//This url returns the list of invalid test scenarios along with their reason of failure
	app.get('/testapi/getAllInvalidTestScenarios', function(req, res) {
		var sanitizedIp = fetchSanitizedIpFromReqObj(req);
		logger.highlight(sanitizedIp+" : Fetched invalid list of test scenarios.");

		res.writeHead(200, {
        'Content-Type': 'application/json'
    	});
		res.end(JSON.stringify(invalidTestScenarioList));
	});

	//This url returns the contents of a requested response file
	app.get('/testapi/showResponseFile', function(req, res) {
		var sanitizedIp = fetchSanitizedIpFromReqObj(req);
		var filename = req.query.file;

		logger.highlight("requested file with name : "+filename);

		var fileToSend = path.join(__dirname, data_dir, filename);
		var pathToSlice = path.join(__dirname, data_dir, "");
		var checker = fileToSend.replace(pathToSlice,"");

		if(pathToSlice == fileToSend)
		{
			logger.warn("Invalid file requisition by IP : "+sanitizedIp);
			res.send("Your IP is logged as "+sanitizedIp+". You do not seem to be needing anything is what your url tells me. Check your url and try again.");
			return;
		}


		if(fileToSend.indexOf(pathToSlice)!=0)
		{
			logger.error("VIOLATION OF FILE ACCESS BY IP : "+sanitizedIp);
			res.send("Your IP is logged as "+sanitizedIp+". Thought for the day : Somethings are better left unseen. ;) Why dont you get a life if you dont agree?");
			return;
		}

		var fs = require('fs');
		logger.info("Accessing "+fileToSend);
		fs.readFile(fileToSend, 'utf8', function (err, data) {

			if (err)
			{
				res.send("Oops! Something went wrong. Your IP is logged as "+sanitizedIp+". You might want to report this incident along with the url you tried to access. This could also mean, the resource you were trying to access is not available anymore.");
				return;
			}

			try
			{
				JSON.stringify(JSON.parse(data));
			}
			catch(e)
			{
				res.send("File contents is not a valid json.");
				return;
			}

			var htmlHead ='<html class="no-js"><head><meta charset="utf-8"><meta http-equiv="cache-control" content="no-cache"><meta http-equiv="expires" content="0"><meta http-equiv="pragma" content="no-cache"><style type="text/css">pre {outline: 0px solid #ccc; padding: 5px; margin: 5px; }.string { color: green; }.number { color: darkorange; }.boolean { color: blue; }.null { color: magenta; }.key { color: red; }</style></head><body>';
			var header = "<h3>Contents of "+filename+"</h3>";
			var htmlTail = "</body></html>"
			data = syntaxHighlight(data);

			res.send(htmlHead+header+data+htmlTail);
		});
	});


	//This url maps a requested test scenario with the incoming ip
	app.post('/testapi/setTestScenario', function(req, res) {
		var sanitizedIp = fetchSanitizedIpFromReqObj(req);
		var requestedId = req.body.id;

		res.writeHead(200, {
        'Content-Type': 'application/json'
    	});
    	var response = {};
    	response.statusCode = "INVALID_ID";

		if(requestedId == null)
		{
			logger.error(sanitizedIp+" : Attempting to set test scenario with invalid test id : "+requestedId+". Operation not permitted.");
			res.end(JSON.stringify(response));
		}
		else if(userDefinedTestScenariosObj[requestedId] == null)
		{
			logger.error(sanitizedIp+" : Attempting to set test scenario with invalid test id : "+requestedId+". Operation not permitted.");
			res.end(JSON.stringify(response));
		}
		else
		{
			logger.highlight(sanitizedIp+" : Attempting to set test scenario with id : "+userDefinedTestScenariosObj[requestedId].id);
			sanitizeAndSetTestScenarioForCurrentIp(userDefinedTestScenariosObj[requestedId], sanitizedIp);
			response.statusCode = "SUCCESS";
		}

		res.end(JSON.stringify(response));
	});

	//This url validates a test scenario from the GUI
	app.post('/testapi/validateTestScenario', function(req, res) {
		var sanitizedIp = fetchSanitizedIpFromReqObj(req);
		var content = req.body.content;
		res.writeHead(200, {
        'Content-Type': 'application/json'
    	});

		logger.info("Validating test scenario from :: "+sanitizedIp);
		logger.warn("Validating content : "+content);

		var response = validateCurrentTestScenarioObj("Pasted Content", content);
		if(response.status == true)
		{
			response = validateAllUrlsInTestScenario(JSON.parse(content));
		}
		res.end(JSON.stringify(response));
	});

	//This url activates a specific test case requested from the GUI
	app.post('/testapi/goToTestCase', function(req, res) {
		var sanitizedIp = fetchSanitizedIpFromReqObj(req);
		var direction = req.body.direction;
		var testScenarioId = req.body.testScenarioId;
		var response = {};
    	response.statusCode = "INVALID_ID";
		res.writeHead(200, {
        'Content-Type': 'application/json'
    	});
    	// console.log(direction, testScenarioId);

		if(direction == "previous")
		{
			logger.log(sanitizedIp+" ::::: Re-enabling previous test case.");
			var lastActiveSequence = getLastActiveSequenceFromActiveTestScenario(req);
			var mapperObject = getIpMapperObject()[sanitizedIp];
			// console.log("PRE"+JSON.stringify(mapperObject.historyWebApis));
			if(mapperObject.historyWebApis.length > 0)
			{
				lastActiveSequence.isExecuted = false;
				mapperObject.activeWebApis = mapperObject.historyWebApis.pop();
				mapperObject.activeWebApisStatusCodes = mapperObject.historyWebApisStatusCodes.pop();
			}
			response.statusCode = "SUCCESS";
			// console.log("POST"+JSON.stringify(mapperObject.historyWebApis));
		}
		else if(direction == "next")
		{
			logger.log(sanitizedIp+" ::::: Skipping to next test case.");
			var currentActiveSequence = getCurrentActiveSequenceFromActiveTestScenario(req);
			var method = currentActiveSequence.expect.method;
			var url = currentActiveSequence.expect.url;
			var response = getResponseForActiveScenario(method, url, req, res);
			response.statusCode = "SUCCESS";
		}
		else
		{
			//Check the test scenario id, if they dont match, activate the requested test scenario id.
			var activeScenario = getIpMapperObject()[sanitizedIp];
			if(isNaN(direction) || direction < 0 || isNaN(testScenarioId))
			{
				response.statusCode = "INVALID_PARAMETER";
				res.end(JSON.stringify(response));
				return;
			}

			var index = parseInt(direction);
			var requestedId = parseInt(testScenarioId);
			// console.log("index :: "+index);
			// console.log("requestedId :: "+requestedId);

			if(requestedId == activeScenario.activeTestScenario.id)
			{
				//User is in the currently activated test scenario.
				//Simply activate the requested test case by index
				if(activeScenario.activeTestScenario.testScenario.totalTestCases-1 >= index)
				{
					activateTestCaseWithIndex(req, res, sanitizedIp, index);
					response.statusCode = "SUCCESS";
				}
				else
				{
					response.statusCode = "INVALID_TESTCASE_INDEX";
					res.end(JSON.stringify(response));
					return;
				}
			}
			else
			{
				//User wants to change the active test scenario.
				//Activate the requested test scenario if it is valid.
				if(requestedId == null)
				{
					logger.error(sanitizedIp+" : Attempting to set test scenario with invalid test id : "+requestedId+". Operation not permitted.");
					res.end(JSON.stringify(response));
				}
				else if(userDefinedTestScenariosObj[requestedId] == null)
				{
					logger.error(sanitizedIp+" : Attempting to set test scenario with invalid test id : "+requestedId+". Operation not permitted.");
					res.end(JSON.stringify(response));
				}
				else
				{
					//If the requested test scenario is valid. Check whether it contains the requested test case.
					if(userDefinedTestScenariosObj[requestedId].testScenario.totalTestCases-1 < index)
					{
						response.statusCode = "INVALID_TESTCASE_INDEX";
						res.end(JSON.stringify(response));
						return;
					}
					logger.highlight(sanitizedIp+" : Attempting to set test scenario with id : "+userDefinedTestScenariosObj[requestedId].id);
					sanitizeAndSetTestScenarioForCurrentIp(userDefinedTestScenariosObj[requestedId], sanitizedIp);
					response.statusCode = "SUCCESS";
					activateTestCaseWithIndex(req, res, sanitizedIp, index);
				}

				logger.log(sanitizedIp+" ::::: activating testcase no :"+direction+" of test scenario Id : "+testScenarioId);
			}
		}

		res.end(JSON.stringify(response));
	});

	function activateTestCaseWithIndex(req, res, sanitizedIp, requestedIndex)
	{
		//Get the currently active index
		var currentActiveSequence = getCurrentActiveSequenceFromActiveTestScenario(req);

		// console.log("lastActiveSequence ::: "+JSON.stringify(lastActiveSequence));
		var currentActiveIndex = 0;
		var mapperObject = null;
		if(currentActiveSequence != null)
		{
			currentActiveIndex = currentActiveSequence.index;
		}

		// console.log("currentActiveIndex :: "+currentActiveIndex);
		// console.log("requestedIndex :: "+requestedIndex);

		if(currentActiveIndex >= requestedIndex)
		{
			if(currentActiveSequence.isExecuted == true)
			{
				//If the currently active sequence is already executed, (When all test cases are completely executed) 
				//we assume the next non existent test case is active, and assign its index
				//by simply incrementing the current active sequence's index
				currentActiveIndex++;
			}
			// console.log("Moving up ");
			//Pop history states appropriately and assign to active web urls
			while(currentActiveIndex != requestedIndex && currentActiveIndex >= requestedIndex)
			{
				mapperObject = getIpMapperObject()[sanitizedIp];
				// if(mapperObject.historyWebApis.length)
				// {
				// 	return;
				// }
				currentActiveSequence = getLastActiveSequenceFromActiveTestScenario(req);
				currentActiveIndex = currentActiveSequence.index;
				currentActiveSequence.isExecuted = false;
				// console.log("History length before pop: "+mapperObject.historyWebApis.length);
				mapperObject.activeWebApis = mapperObject.historyWebApis.pop();
				mapperObject.activeWebApisStatusCodes = mapperObject.historyWebApisStatusCodes.pop();
				// console.log("Current active marked false at index : "+currentActiveSequence.index);
				// console.log("History length after pop: "+mapperObject.historyWebApis.length);
				// console.log("currentActiveIndex :: "+currentActiveIndex);
			}
		}
		else
		{
			// console.log("Moving down ");			
			while(currentActiveIndex != requestedIndex && currentActiveIndex < requestedIndex)
			{
				//Activate the requested index
				//Check the test case index. If it does not pass, do not activate.
				var method = currentActiveSequence.expect.method;
				var url = currentActiveSequence.expect.url;
				var response = getResponseForActiveScenario(method, url, req, res);
				mapperObject = getIpMapperObject()[sanitizedIp];
				// console.log("History length : "+mapperObject.historyWebApis.length);
				currentActiveSequence = getCurrentActiveSequenceFromActiveTestScenario(req);
				currentActiveIndex = currentActiveSequence.index;
				// console.log("currentActiveIndex :: "+currentActiveIndex);
			}
		}
	}

	try
	{
		logger.info("Attempting to start stub server.");
		app.listen(STUB_SERVER_PORT);
		logger.highlight("Stub server is listening at port : "+STUB_SERVER_PORT);
	}
	catch(e)
	{
		logger.error("Failed to start the stub server. Reason : "+e);
	}
}
