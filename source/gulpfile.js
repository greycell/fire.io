'use strict';

var gulp = require('gulp');
var gulpWar = require('gulp-war');
var gulpZip = require('gulp-zip');
var stripDebug = require('gulp-strip-debug');

gulp.paths = {
  src: 'src',
  dist: 'dist',
  bower_components: 'bower_components',
  tmp: '.tmp',
  rel: 'release',
  assets:'src/assets'
};

require('require-dir')('./gulp');

gulp.task('strip-debug', ['buildapp'], function () {
  return gulp.src(["dist/**/app*.js"])
    .pipe(stripDebug())
    .pipe(gulp.dest('dist'));
});

gulp.task('build-war', ['strip-debug'], function () {

console.log("BUILDING WAR FILE");
return gulp.src(["dist/**/**"])
        .pipe(gulpWar({
            welcome: 'index.html',
            displayName: 'Twuizt WAR',
        }))
        .pipe(gulpZip('TwuiztGUI.war'))
        .pipe(gulp.dest(gulp.paths.rel));
});

gulp.task('production',['clean'],function(){
	gulp.start('build-war');
});
