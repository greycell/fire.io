'use strict';

var gulp = require('gulp');

var paths = gulp.paths;

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', function () {
  return gulp.src([
    paths.src + '/{app,components}/**/*.html',
    paths.tmp + '/{app,components}/**/*.html'
  ])
    .pipe($.if(function(file) {
        return $.match(file, ['!**/examples/*.html']);
      },
      $.minifyHtml({
        empty: true,
        spare: true,
        quotes: true
      }))
    )
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'twuizt'
    }))
    .pipe(gulp.dest(paths.tmp + '/partials/'));
});

gulp.task('html', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(paths.tmp + '/partials/templateCacheHtml.js', { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: paths.tmp + '/partials',
    addRootSlash: false
  };

  var htmlFilter = $.filter(['*.html', '!/src/app/elements/examples/*.html']);
  var jsFilter = $.filter('**/*.js');
  var cssFilter = $.filter('**/*.css');
  var assets;

  return gulp.src(paths.tmp + '/serve/index.html')
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    .pipe($.rev())
    .pipe(jsFilter)
    .pipe($.ngAnnotate())
   // .pipe($.uglify({preserveComments: $.uglifySaveLicense}))
    .pipe(jsFilter.restore())
    .pipe(cssFilter)
    .pipe($.csso())
    .pipe(cssFilter.restore())
    .pipe(assets.restore())
    .pipe($.replace('../bower_components/material-design-icons/iconfont', '../fonts/materialfonts'))
    .pipe($.replace('../bower_components/material-design-iconic-font/fonts', '../fonts/iconicfonts'))
    .pipe($.replace('../bower_components/robotodraft/fonts', '../fonts/robotofonts'))
    .pipe($.replace('../bower_components/font-awesome/fonts', '../fonts/fontawesome'))
    .pipe($.useref())
    .pipe($.revReplace())
    .pipe(htmlFilter)
    // .pipe($.minifyHtml({
    //   empty: true,
    //   spare: true,
    //   quotes: true
    // }))
    .pipe(htmlFilter.restore())
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size({ title: paths.dist + '/', showFiles: true }))
    .on('error', onError);;
});


// Handle an error based on its severity level.
// Log all levels, and exit the process for fatal levels.
function handleError(level, error) {
   gutil.log(error.message);
   if (isFatal(level)) {
      process.exit(1);
   }
}

// Convenience handler for error-level errors.
function onError(error) { handleError.call(this, 'error', error);}
// Convenience handler for warning-level errors.
function onWarning(error) { handleError.call(this, 'warning', error);}


gulp.task('assets', function () {
  return gulp.src(paths.src + '/assets/**/*')
    .pipe(gulp.dest(paths.dist + '/assets/'));
});

gulp.task('fonts', ['font-awesome', 'roboto-fonts', 'material-icon-fonts', 'material-design-iconic-fonts']);

gulp.task('font-awesome', function () {
  // return gulp.src($.mainBowerFiles())
  var mainFontFiles = $.mainBowerFiles();
  mainFontFiles.push("bower_components/font-awesome/fonts/**/*");
  console.log("font folder : "+JSON.stringify(mainFontFiles));
  return gulp.src(mainFontFiles)
    .pipe($.filter('**/*.{eot,otf,svg,ttf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest(paths.dist + '/fonts/fontawesome/'));
});

gulp.task('roboto-fonts', function () {

  var robotoFontFiles = [];
  robotoFontFiles.push("bower_components/robotodraft/fonts/**");
  console.log("font folder : "+JSON.stringify(robotoFontFiles));
  return gulp.src(robotoFontFiles)
    .pipe($.filter('**/*.{eot,otf,svg,ttf,woff,woff2}'))
    .pipe(gulp.dest(paths.dist + '/fonts/robotofonts/'));
});

gulp.task('material-icon-fonts', function () {

  var materialFontFiles = [];
  materialFontFiles.push("bower_components/material-design-icons/iconfont/**/*");
  console.log("font folder : "+JSON.stringify(materialFontFiles));
  return gulp.src(materialFontFiles)
    .pipe($.filter('**/*.{eot,otf,svg,ttf,woff,woff2}'))
    .pipe(gulp.dest(paths.dist + '/fonts/materialfonts/'));
});

gulp.task('material-design-iconic-fonts', function () {

  var materialFontFiles = [];
  materialFontFiles.push("bower_components/material-design-iconic-font/fonts/**/*");
  console.log("font folder : "+JSON.stringify(materialFontFiles));
  return gulp.src(materialFontFiles)
    .pipe($.filter('**/*.{eot,otf,svg,ttf,woff,woff2}'))
    .pipe(gulp.dest(paths.dist + '/fonts/iconicfonts/'));
});


gulp.task('translations', function () {
  return gulp.src('src/**/il8n/*.json')
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size());
});

gulp.task('data', function () {
  return gulp.src('src/**/data/*.json')
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size());
});

gulp.task('examplejs', function () {
  return gulp.src('src/**/examples/*.{js,scss}')
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size());
});

gulp.task('jshint', function() {
  return gulp.src('src/**/*.js')
    .pipe($.jshint())
    .pipe($.jshint.reporter('default'));
});


gulp.task('misc', function () {
  return gulp.src(paths.src + '/favicon.png')
    .pipe(gulp.dest(paths.dist + '/'));
});


gulp.task('web-inf', function () {
  return gulp.src(paths.src + '/WEB-INF/**/*')
    .pipe(gulp.dest(paths.dist + '/WEB-INF/'));
});

gulp.task('clean', function (done) {
  return $.del([paths.dist + '/', paths.tmp + '/', paths.rel + '/'], done);
});

gulp.task('buildapp', ['html', 'assets', 'fonts', 'translations', 'misc', 'data', 'examplejs', 'web-inf']);
