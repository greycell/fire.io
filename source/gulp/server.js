'use strict';

var gulp = require('gulp');

var paths = gulp.paths;

var util = require('util');

var browserSync = require('browser-sync');

var middleware = require('./proxy.js');

var compress = require('compression');

function browserSyncInit(baseDir, files, browser, isProduction) {

    console.info("isProduction ?? " + isProduction);

    browser = browser === undefined ? 'default' : browser;

    var routes = null;
    // if(baseDir === paths.src || (util.isArray(baseDir) && baseDir.indexOf(paths.src) !== -1)) {
    routes = {
        '/bower_components': 'bower_components',
        '/test': 'src/test'
    };
    var config = null;
    // }
    if (isProduction) {
        config = {
            startPath: '/',
            server: {
                baseDir: baseDir,
                middleware: middleware,
                routes: routes
            },
            ghostMode: {
                clicks: false,
                forms: false,
                scroll: false
            },

            port: 4000,
            minify: false
        };
    } else {
        config = {
            // https:true,
            // https: {
            //     key: "./src/twuizt.key",
            //     cert: "./src/twuizt.crt"
            // },
            startPath: '/',
            server: {
                baseDir: baseDir,
                middleware: middleware,
                routes: routes
            },
            ghostMode: {
                clicks: false,
                forms: false,
                scroll: false
            },
            port: 3000,
            minify: false
        };


    }
    browserSync.instance = browserSync.init(files, config);
}

gulp.task('serve:production', ['clean'], function() {
    gulp.start('production-via-browsersync');
});

gulp.task('production-via-browsersync', ['build-war'], function() {
    browserSyncInit([paths.dist], null, [], true);
});

gulp.task('serve:development', ['watch'], function() {
    browserSyncInit([
        paths.tmp + '/serve',
        paths.src
    ], [
        paths.tmp + '/serve/{app,components}/**/*.css',
        paths.src + '/{app,components}/**/*.js',
        paths.src + 'src/assets/images/**/*',
        paths.tmp + '/serve/*.html',
        paths.tmp + '/serve/{app,components}/**/*.html',
        paths.src + '/{app,components}/**/*.html'
    ], null, false);
});