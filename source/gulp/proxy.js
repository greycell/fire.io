 /*jshint unused:false */

 'use strict';


 var httpProxy = require('http-proxy');
 var chalk = require('chalk');
 var compress = require('compression');

 var proxyServerAddress = "localhost:8888"

 var proxyWebapiTarget = 'http://' + proxyServerAddress;

 var proxyForWebapi = httpProxy.createProxyServer({
     target: proxyWebapiTarget
 });
 proxyForWebapi.on('proxyReq', function(proxyReq, req, res, options) {
     proxyReq.setHeader('OriginalIp', req.connection.remoteAddress);
     // res.setHeader('Content-Encoding', 'gzip');
 });


 proxyForWebapi.on('error', function(error, req, res) {
     res.writeHead(500, {
         'Content-Type': 'text/plain'
     });

     console.error(chalk.red('[Proxy]'), error);
 });


 var proxySocketTarget = 'http://' + proxyServerAddress;

 var proxyForSocket = httpProxy.createProxyServer({
     target: proxySocketTarget,
     ws:true
 });
 proxyForSocket.on('proxyReq', function(proxyReq, req, res, options) {
     proxyReq.setHeader('OriginalIp', req.connection.remoteAddress);
     // res.setHeader('Content-Encoding', 'gzip');
 });


 proxyForSocket.on('error', function(error, req, res) {
     res.writeHead(500, {
         'Content-Type': 'text/plain'
     });

     console.error(chalk.red('[Proxy]'), error);
 });


 /*
  * The proxy middleware is an Express middleware added to BrowserSync to
  * handle backend request and proxy them to your backend.
  */
 function proxyMiddleware(req, res, next) {
     /*
      * This test is the switch of each request to determine if the request is
      * for a static file to be handled by BrowserSync or a backend request to proxy.
      *
      * The existing test is a standard check on the files extensions but it may fail
      * for your needs. If you can, you could also check on a context in the url which
      * may be more reliable but can't be generic.
      */
     // if (/\.(html|css|js|png|jpg|jpeg|gif|ico|xml|rss|txt|eot|svg|ttf|woff|cur)(\?((r|v|rel|rev)=[\-\.\w]*)?)?$/.test(req.url)) {

     if ((req.url).indexOf("/twuiztApp/") != -1)
     // if ((req.url).indexOf("/twuiztApp/") == -1)
     {
         console.log("request for TwuiztAPP " + req.url + " from ip :" + req.connection.remoteAddress + " is routed to " + proxyWebapiTarget);
         proxyForWebapi.web(req, res);
     } 

     else if ((req.url).indexOf("/twuiztSocket/") != -1) 
     {
         console.log("request for SOCKET " + req.url + " from ip :" + req.connection.remoteAddress + " is routed to " + proxyWebapiTarget);
         proxyForSocket.web(req, res);
     } 
     else 
     {
         // console.log("No webapi in the url");
         // console.log("req :: "+JSON.stringify(req));
         var gzip = compress();
         gzip(req, res, next);
         // next();
     }
 }

 /*
  * This is where you activate or not your proxy.
  */

 module.exports = proxyMiddleware;
 //module.exports = [];
