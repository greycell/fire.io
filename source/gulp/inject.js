'use strict';

var gulp = require('gulp');
var debug = require('gulp-debug');
var paths = gulp.paths;
var jshint = require('gulp-jshint');

var $ = require('gulp-load-plugins')();

var wiredep = require('wiredep').stream;

var map = require('map-stream');
 
var myReporter = map(function (file, cb) {
  if (!file.jshint.success) {
    console.log('JSHINT fail in '+file.path);
    file.jshint.results.forEach(function (err) {
      if (err) {
        console.log(' '+file.path + ': line ' + err.line + ', col ' + err.character + ', code ' + err.code + ', ' + err.reason);
      }
    });
  }
  cb(null, file);
});

gulp.task('inject', ['styles'], function () {

  console.log("Injecting styles : "+paths.tmp + '/serve/{app,components}/**/*.css',
    '!' + paths.tmp + '/serve/app/vendor.css');

  var injectStyles = gulp.src([
    paths.tmp + '/serve/{app,components}/**/*.css',
    '!' + paths.tmp + '/serve/app/vendor.css'
  ], { read: false });

  console.log("Injecting scripts : "+paths.src + '/{app,components}/**/*.js',
    '!' + paths.src + '/{app,components}/**/*.spec.js',
    '!' + paths.src + '/{app,components}/**/*.mock.js');

  var injectScripts = gulp.src([
    paths.src + '/{app,components}/**/*.js',
    '!' + paths.src + '/{app,components}/**/*.spec.js',
    '!' + paths.src + '/{app,components}/**/*.mock.js'
  ])
  // .pipe(jshint())
  // .pipe(jshint.reporter('default', { verbose: true }))
  .pipe($.angularFilesort());

  var injectOptions = {
    ignorePath: [paths.src, paths.tmp + '/serve'],
    addRootSlash: false
  };
  
  console.log("Inject options object: "+JSON.stringify(injectOptions));

  var wiredepOptions = {
    directory: 'bower_components',
    exclude: [/bootstrap\.css/, /foundation\.css/, /material-design-iconic-font\.css/, /default\.css/]
  };

  console.log("wiredepOptions object: "+JSON.stringify(wiredepOptions));

  return gulp.src(paths.src + '/*.html')
    // .pipe(jshint())
    // .pipe(jshint.reporter('default', { verbose: true }))
    .pipe(debug({title: 'Inject I :', minimal:false}))
    .pipe($.inject(injectStyles, injectOptions))
    .pipe(debug({title: 'Inject II :', minimal:false}))
    .pipe($.inject(injectScripts, injectOptions))
    .pipe(debug({title: 'Inject III :', minimal:false}))
    .pipe(wiredep(wiredepOptions))
    .pipe(debug({title: 'Inject IV :', minimal:false}))
    .pipe(gulp.dest(paths.tmp + '/serve'));

});
